<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\RuItLanguages;
use app\models\RuLanguage;
use app\models\ItLanguage;

class Category extends ActiveRecord
{
    
    const DEFAULT_CATEGORY = 0;
    
    public static function tableName()
    {
        return 'category';
    }
    
    public function rules()
    {
        return array(
            ['category', 'required']
        );
    }
    
    /*public function relations()
    {
        return array(
            'RuLanguages' => array(self::MANY_MANY, 'RuLanguage', 'ru_it_languages(id_category, id_ru)'),
            'ItLanguages' => array(self::MANY_MANY, 'ItLanguage', 'ru_it_languages(id_category, id_it)'),
            'RuItLanguages' => array(self::HAS_MANY, 'RuItLanguages', 'id_category')
        );
    }*/
    
    public function getRuItLanguages()
    {
        return $this->hasMany(RuItLanguages::className(), ['id' => 'id_category']);
    }
    
    public function getRuLanguages()
    {
        return $this->hasMany(RuLanguage::className(), ['id' => 'id_ru'])
                ->viaTable('ru_it_languages', ['id_category' => 'id']);
    }
    
    public function getItLanguages()
    {
        return $this->hasMany(ItLanguage::className(), ['id' => 'id_it'])
                ->viaTable('ru_it_languages', ['id_category' => 'id']);
    }

}
