<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

class RegistrationForm extends Model
{
    public $firstname;
    //public $lastname;
    //public $country;
    //public $city;
    public $email;
    public $password;
    public $repassword;
    public $sex;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['firstname', 'email', 'password', 'repassword'], 'required'],
            ['email', 'email'],
            ['email', 'unique'],
            //['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.{7,})$/', 'message' => \Yii::t('main', 'The password must be at least 7 characters long including uppercase and lowercase letters, digits')],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'message' => \Yii::t('main', 'Password should be equal')],
        ];
    }
    
    public function registrate() {
        $newUser = new User();
        $newUser->email = $this->email;
        $newUser->setPassword($this->password);
        $newUser->name = $this->firstname;
        $newUser->sex = $this->sex;
        if($newUser->validate()) {
            $newUser->save(false);
            return true;
        }
        return false;
    }
}
