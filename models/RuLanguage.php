<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\ItLanguage;
use app\models\RuItLanguages;
use app\models\Category;

class RuLanguage extends ActiveRecord
{
    public static function tableName()
    {
        return 'ru_language';
    }
    
    public function rules()
    {
        return [
            ['word', 'required'],
            ['word', 'unique']
        ];
    }
    
    /*public function relations()
    {
        return array(
            'ItLanguages' => array(self::MANY_MANY, 'ItLanguage', 'ru_it_languages(id_ru, id_it)'),
            'Translate' => array(self::MANY_MANY, 'ItLanguage', 'ru_it_languages(id_ru, id_it)'),
            'Categories' => array(self::MANY_MANY, 'Category', 'ru_it_languages(id_ru, id_category)'),
        );
    }*/
    
    public function getTranslates()
    {
        return $this->hasMany(ItLanguage::className(), ['id' => 'id_it'])
                ->viaTable('ru_it_languages', ['id_ru' => 'id']);
    }

    public static function setPointsWord($answers){
        if(strlen($answers) == 2 && $answers >=10 && $answers <= 20) {
            $pointsText = Yii::t('main', 'очков');
        } else {
            $points = substr($answers, strlen($answers)-1);
            if($points == 0) {
                $pointsText = Yii::t('main', 'очков');
            } elseif($points == 1) {
                $pointsText = Yii::t('main', 'очко');
            } elseif($points >= 2 && $points <= 4) {
                $pointsText = Yii::t('main', 'очкa');
            } elseif($points >= 5 && $points <= 9) {
                $pointsText = Yii::t('main', 'очков');
            }
        }
        
        return $pointsText;
    }
    
}
