<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Category;
use app\models\ItLanguage;

class RuItLanguages extends ActiveRecord
{
    public static function tableName()
    {
        return 'ru_it_languages';
    }
    
    /*public function relations()
    {
        return array(
            'ItLanguage' => array(self::HAS_MANY, 'ItLanguage', 'id_it'),
            'Category' => array(self::BELONGS_TO, 'Category', 'id')
        );
    }*/
    
    public function getItLanguages()
    {
        return $this->hasMany(ItLanguage::className(), ['id' => 'id_it']);
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id_category' => 'id']);
    }
}
