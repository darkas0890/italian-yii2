<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\RuLanguage;
use app\models\RuItLanguages;
use app\models\Category;

class ItLanguage extends ActiveRecord
{
    public static function tableName()
    {
        return 'it_language';
    }
    
    public function rules()
    {
        return [
            ['word', 'required'],
            ['word', 'unique']
        ];
    }
    
    /*public function relations()
    {
        return array(
            //'RuLanguages' => array(self::MANY_MANY, 'RuLanguage', 'ru_it_languages(id_it, id_ru)'),
            'Translate' => array(self::MANY_MANY, 'RuLanguage', 'ru_it_languages(id_it, id_ru)'),
            //'RuItLanguages' => array(self::BELONGS_TO, 'RuItLanguages', 'id'),
            'Categories' => array(self::MANY_MANY, 'Category', 'ru_it_languages(id_it, id_category)'),
        );
    }*/
    
    public function getTranslates()
    {
        return $this->hasMany(RuLanguage::className(), ['id' => 'id_ru'])
                ->viaTable('ru_it_languages', ['id_it' => 'id']);
    }
}
