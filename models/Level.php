<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\User;

class Level extends ActiveRecord
{
    public static function tableName()
    {
        return 'level';
    }
    
    public function relations()
    {
        return array(
            'User' => array(self::HAS_MANY, 'User', 'id')
        );
    }
    
    public static function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'level']);
    }
}
