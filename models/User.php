<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\web\Linkable;
use yii\db\ActiveRecord;
use yii\base\NotSupportedException;
use app\models\Level;
use app\models\Activity;

class User extends ActiveRecord implements IdentityInterface
{

    const ROLE_USER = 0;
    const ROLE_MODERATOR = 1;
    const ROLE_ADMIN = 2;
    const ROLE_SUPERADMIN = 3;
    
    public static function tableName()
    {
        return 'user';
    }
    public function rules()
    {
        return [
            [['name', 'email', 'password_hash'], 'required'],
            ['password_hash', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.{7,})$/', 'message' => \Yii::t('main', 'The password must be at least 7 characters long including uppercase and lowercase letters, digits')],
            ['name', 'string', 'max' => 32, 'tooLong' => \Yii::t('main', 'Name is too long (maximum is {max} characters)', array(32))],
            ['name', 'match', 'pattern' => '/^[\w\.\s\-\,]+$/u', 'message' => 'Only following characters are allowed in Name: "A-Z", "0-9", "_", ".", "-", ","'],
            ['email', 'email'],
        ];
    }
    
    public function getLevels()
    {
        return $this->hasOne(Level::className(), ['id' => 'level']);
    }
    
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['user_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function getUser()
    {
        return User::findIdentity(Yii::$app->user->identity->id);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
}
