<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Activity extends ActiveRecord
{
    public static function tableName()
    {
        return 'activity';
    }
    
    public static function toolTipProgress() {
        $activityModel = Activity::find()
                ->where(['user_id' => Yii::$app->user->identity->id, 'date' => date('Y-m-d')])
                ->one();
        $totalExperience = Yii::$app->user->identity->levels->experience;
        $experienceLeft = $totalExperience - Yii::$app->user->identity->experience;
        $days = $activityModel->gain_experience > 0 ? ceil($experienceLeft / $activityModel->gain_experience) - 1 : $experienceLeft - 1;
        $gainTextDefault = \Yii::t('main', 'С такими успехами как сегодня следующий уровень сможешь получить ');
        if($days == 0) {
            $gainText = "<b>" .\Yii::t('main', 'в этот же день') . "</b>!";
        } elseif($days == 1) {
            $gainText = "<b>" . \Yii::t('main', 'завтра') . "</b>!";
        } else {
            if(strlen($days-1) >= 2) {
                $gainText = \Yii::t('main', 'более чем') . " " . \Yii::t('main', 'через') . " <b>10 " . \Yii::t('main', 'дней') . "</b> ";
            } else {
                $daysEnd = substr($days-1, strlen($days-1)-1);
                if($daysEnd == 1) {
                    $gainText = \Yii::t('main', 'через') . " <b>" . ($days - 1) . " " . \Yii::t('main', 'день') . "</b>!";
                } elseif($daysEnd >= 2 && $daysEnd <= 4) {
                    $gainText = \Yii::t('main', 'через') . " <b>" . ($days - 1) . " " . \Yii::t('main', 'дня') . "</b>!";
                } elseif($daysEnd >= 5 && $daysEnd <= 9) {
                    $gainText = \Yii::t('main', 'через') . " <b>" . ($days - 1) . " " . \Yii::t('main', 'дней') . "</b>!";
                }
            }
        }
        return $gainTextDefault . $gainText;
    }

}