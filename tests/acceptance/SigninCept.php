<?php 
$I = new AcceptanceTester($scenario);
$I->am('user'); // actor's role
$I->wantTo('login to website'); // feature to test
$I->lookForwardTo('access all website features'); // result to achieve
$I->amOnPage('/authorization');
$I->click('div.form_registration_button');
$I->see('Вход', '.form_selected');
$I->fillField('LoginForm[email]','maninaglushchenko@mail.ru');
$I->fillField('LoginForm[password]','123');
$I->see('Войти', '#login-form');
$I->click('Войти');
$I->submitForm('#login-form', []);
//$I->amOnPage('/site/home');
//$I->seeInCurrentUrl('/site/home');
//$I->see('Маришка');
$I->see('Необходимо заполнить «Email».', '.help-block');