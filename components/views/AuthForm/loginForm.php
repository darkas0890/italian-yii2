<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\LoginForm;

$model = new LoginForm;
?>
<div class="auth_title">
    <h3><?= Html::encode($title); ?></h3>
</div>
<div class="form-container">
    <?php $form = ActiveForm::begin(
            ['id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'validateOnSubmit' => true,
                'validateOnType' => false,
                'validateOnChange' => false,
                'validateOnBlur' => false
                ]); ?>

    <?= $form->field($model, 'email')->input('email', ['class' => 'form-control login_email', 'autocomplete' => 'off', 'placeholder' => 'E-mail'])->label(false); ?>
    <?= $form->field($model, 'password')->input('password', ['class' => 'form-control login_password', 'autocomplete' => 'off', 'placeholder' => 'Пароль'])->label(false); ?>
    <?= Html::submitButton('Войти', ['class' => 'login_submit', 'value' => 'Войти']); ?>
    <?php ActiveForm::end(); ?>
</div>