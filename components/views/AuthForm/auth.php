<?php
use yii\helpers\Html;
use app\components\assets\AuthFormAsset;

AuthFormAsset::register($this);
?>
<div class="auth <?= $startForm == 'login' ? 'auth_login' : 'auth_registration'; ?>">
    <div class="auth_inner">
        <div class="change_form">
            <div class="form_login_button <?= $startForm == 'login' ? 'form_selected' : ''; ?>">Вход</div>
            <div class="form_registration_button <?= $startForm == 'registration' ? 'form_selected' : ''; ?>">Регистрация</div>
        </div>
        <div class="form_render">
            <div class="auth_login_tab <?= $startForm == 'login' ? '' : 'hidden_tab'; ?>">
                <?= $this->render('loginForm', ['title' => 'Вход']); ?>
            </div>
            <div class="auth_registration_tab <?= $startForm == 'registration' ? '' : 'hidden_tab'; ?>">
                <?= $this->render('registrationForm', ['title' => 'Регистрация']); ?>
            </div>
        </div>
    </div>
</div>