<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\RegistrationForm;

$model = new RegistrationForm;
?>
<div class="auth_title">
    <h3><?= Html::encode($title); ?></h3>
</div>
<div class="form-container">
    <?php $form = ActiveForm::begin(
            ['id' => 'registration-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'validateOnSubmit' => true,
                'validateOnType' => false,
                'validateOnChange' => false,
                'validateOnBlur' => false
                ]); ?>
    <?php $model->sex = 1; ?>
    <?= $form->field($model, 'sex')->radioList(['Мужчина','Женщина'])->label(false); ?>
    <?= $form->field($model, 'firstname')->textInput(['class' => 'form-control registration_firstname', 'autocomplete' => 'off', 'placeholder' => 'Имя'])->label(false); ?>
    <?= $form->field($model, 'email')->input('email', ['class' => 'form-control registration_email', 'autocomplete' => 'off', 'placeholder' => 'E-mail'])->label(false); ?>
    <?= $form->field($model, 'password')->input('password', ['class' => 'form-control registration_password', 'autocomplete' => 'off', 'placeholder' => 'Пароль'])->label(false); ?>
    <?= $form->field($model, 'repassword')->input('password', ['class' => 'form-control registration_repassword', 'autocomplete' => 'off', 'placeholder' => 'Повторить пароль'])->label(false); ?>
    <?= Html::submitButton('Зарегистрироваться', ['class' => 'registration_submit']); ?>
    <?php ActiveForm::end(); ?>
</div>