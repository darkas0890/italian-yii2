$(document).ready(function() {
    
    
    $('.auth').on('click', '.change_form', function() {
        $('.form_registration_button').toggleClass('form_selected');
        $('.form_login_button').toggleClass('form_selected');
        
        $('.auth').toggleClass('auth_login');
        $('.auth').toggleClass('auth_registration');
        
        
        $('.form_render').fadeOut(300, function() {
            $("#login-form").yiiActiveForm('resetForm');
            $("#registration-form").yiiActiveForm('resetForm');
            $("#login-form input, #registration-form input").val('');
            
            //Дикий костыль ====================
            $('label:first-child input[type="radio"]').val('male');
            $('label:last-child input[type="radio"]').val('female');
            //Дикий костыль ====================
            
            $('.auth_login_tab').toggleClass('hidden_tab');
            $('.auth_registration_tab').toggleClass('hidden_tab');
            $('.form_render').fadeIn(500);
            
            $('input[type="radio"]').removeAttr('checked');
            $('label:first-child input[type="radio"]').prop('checked', 'checked');
            
            setCheckedStyle();
        });
    });
    
    
    setCheckedStyle();
    $('input[type="radio"]').on('change', function() {
        setCheckedStyle();
    });
});

function setCheckedStyle()
{
    $('input[type="radio"]').parent().attr('style', '');
    $('input[type="radio"]:checked').parent().css('background-color', '#d0d0d0');
    $('input[type="radio"]:checked').parent().css('background-image', 'none');
}