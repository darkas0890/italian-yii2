<?php

namespace app\components\assets;

use yii\web\AssetBundle;

class AuthFormAsset extends AssetBundle {
    public $sourcePath = '@app/components/assets';
    public $css = [
        'css/AuthFormStyle.css'
        ];
    public $js = [
        'js/AuthFormScript.js'
        ];
    public $publishOptions = [
        'forceCopy' => true
        ];
}