<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use app\components\assets\AuthFormAsset;

class AuthFormWidget extends Widget
{
    public $startForm = 'login';
    
    private $title = 'Вход';
    
    public function init() {
        parent::init();
        if($this->startForm == 'login') {
            $this->title = 'Вход';
        } elseif($this->startForm == 'registration') {
            $this->title = 'Регистрация';
        } else {
            throw new Exception("Wrong form");
        }
    }
    
    public function run() {
        return $this->render('AuthForm/auth', [
            'startForm' => $this->startForm
        ]);
    }
}