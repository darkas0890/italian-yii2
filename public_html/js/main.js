jQuery(document).ready(function () {
    $(".select2").select2();
    $('[data-toggle="popover"]').popover();
    setTimeout(setLevelProgress, 200);
    
    toggleExperience(false);
    setTimeout(function() { toggleLevel(false) }, 900);
    
    $('input[type="submit"]').mousedown(function(){
        $(this).css('background', '#2ecc71');
    });
    $('input[type="submit"]').mouseup(function(){
        $(this).css('background', '#1abc9c');
    });
    $('#loginform').click(function(){
        $('.login').fadeToggle('slow');
        $(this).toggleClass('green');
    });
    
    $(document).mouseup(function (e){
        var container = $(".login");
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
            $('#loginform').removeClass('green');
        }
    });
    
    $('.header-avatar').hover(function() {
        $(this).addClass('profile-popup_visible');}, function() {
        $(this).removeClass('profile-popup_visible');
    });
    
    $( ".cross" ).hide();
    $( ".menu-mobile" ).hide();
    $( ".hamburger" ).click(function() {
        $( ".menu-mobile" ).fadeIn( 300, function() {
            $( ".hamburger" ).hide();
            $( ".cross" ).show();
        });
    });

    $( ".cross" ).click(function() {
        $( ".menu-mobile" ).fadeOut( 300, function() {
            $( ".cross" ).hide();
            $( ".hamburger" ).show();
        });
    });
    
});

function setLevelProgress() {
    var progress = $(".profile-experience-progress").data('experience-progress'),
        total = $(".profile-experience-progress").data('experience-total');
    $(".profile-experience-progress").css("transition", "2s cubic-bezier(0.87,-0.01, 0.23, 0.93)");
    $(".profile-experience-progress").css("width", progress*100/total + "%");
}

function toggleExperience(deleteMessage) {
    if($(document).find(".flash-gained-experience").length) {
        $(document).find(".flash-gained-experience").toggleClass("flash-gained-experience-display");
    }
    if(deleteMessage) {
        setTimeout(function() { $(document).find(".flash-gained-experience").remove(); }, 1000);
    } else {
        setTimeout(function() { toggleExperience(true); }, 5500);
    }
}

function toggleLevel(deleteMessage) {
    if($(document).find(".flash-gained-level").length) {
        $(document).find(".flash-gained-experience").toggleClass("flash-gained-experience-level-up");
        $(document).find(".flash-gained-level").toggleClass("flash-gained-level-display");
    }
    if(deleteMessage) {
        setTimeout(function() { $(document).find(".flash-gained-level").remove();  }, 1000);
    } else {
        setTimeout(function() { toggleLevel(true); }, 4000);
    }
}

function parse(val) {
    var result = "Not found",
        tmp = [];
    location.search
    .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
    });
    return result;
}

function parseLanguageAndCategory() {
    var temp = ['', false],
        root = location.protocol + '//' + location.host,
        url = location.href,
        afterRoot = url.replace(root,'').substr(1).split("/");
    
    afterRoot
        .forEach(function (item, index) {
            if(item == 'ru' || item == 'it')
                temp[0] = item;
            
            if(index == afterRoot.length - 1 && $.isNumeric(item))
                temp[1] = item;
        });
        
    return temp;
}
