var idIntervalRound;
var idInterval;
$(document).ready(function() {
    console.log(words.length);
    idIntervalRound = roundTimeOut($('.training-round'));
    idInterval = setInterval(function() { timeOut(idInterval, idIntervalRound) }, 1000);
    
    $('.training-inner').on('click', '.training-translate-list li:not(.last) a', function(event) {
        event.preventDefault();
        clearInterval(idInterval);
        clearInterval(idIntervalRound);
        var isRight = false;
        if($('.training-translate-list li a.answer-success').length) {
            return;
        }
        var chosenAnswerId = $(this).parent().attr('data-answer-item');
        var successAnswerId = $('h3.training-word').attr('data-answer-item');
        if(chosenAnswerId == successAnswerId) {
            $(this).addClass('answer-success');
            isRight = true;
        } else {
            $(this).addClass('answer-fail');
            $('.training-translate-list li').each(function(elem, index) {
                if($(this).attr('data-answer-item') == successAnswerId) {
                    $(this).find('a').addClass('answer-success');
                }
            });
        }
        $("li.last a b").html('Следующее');
        //setTimeout(newAnswer(SuccessAnswerId, isRight), 400);
    });
    
    $('.training-inner').on('click', '#skip-item', function(event) {
        clearInterval(idInterval);
        clearInterval(idIntervalRound);
        var successAnswerId = $('h3.training-word').attr('data-answer-item');
        if(!$('.training-translate-list li a.answer-success').length) {
            $('.training-translate-list').find('li[data-answer-item="' + successAnswerId + '"] a').addClass('answer-success');
            $('.training-translate-list').find('li[data-answer-item="' + successAnswerId + '"] a').addClass('answer-skipped');
            $("li.last a b").html('Следующее');
            return;
        }
        $('.training-inner').find('#skip-item').removeAttr('id');
        var isRight = $('.training-translate-list li a.answer-fail').length || $('.training-translate-list li a.answer-skipped').length ? false : true;
        newAnswer(successAnswerId, isRight);
    });
    
    $(document).bind('keyup', function(event) {
        if(event.keyCode == 13 && $('.training-inner').find('#skip-item').length) {
            clearInterval(idInterval);
            clearInterval(idIntervalRound);
            var successAnswerId = $('.training-inner').find('h3.training-word').attr('data-answer-item');
            if(!$('.training-translate-list li a.answer-success').length) {
                $('.training-translate-list').find('li[data-answer-item="' + successAnswerId + '"] a').addClass('answer-success');
                $('.training-translate-list').find('li[data-answer-item="' + successAnswerId + '"] a').addClass('answer-skipped');
                $("li.last a b").html('Следующее');
                return;
            }
            $('.training-inner').find('#skip-item').removeAttr('id');
            var isRight = $('.training-translate-list li a.answer-fail').length || $('.training-translate-list li a.answer-skipped').length ? false : true;
            newAnswer(successAnswerId, isRight);
        }
        if(event.keyCode == 49) {
             $('.training-inner .training-translate-list li:nth-child(1) a').click();
        }
        if(event.keyCode == 50) {
             $('.training-inner .training-translate-list li:nth-child(2) a').click();
        }
        if(event.keyCode == 51) {
             $('.training-inner .training-translate-list li:nth-child(3) a').click();
        }
        if(event.keyCode == 52) {
             $('.training-inner .training-translate-list li:nth-child(4) a').click();
        }
        if(event.keyCode == 53) {
             $('.training-inner .training-translate-list li:nth-child(5) a').click();
        }
    });
});

function newAnswer(answerId, isRight) {
    console.time('trigger');
    
    var totalProgress = $('span.training-total-progress-number').html();
    var data = (parseInt($('.training-inner').find('h3.training-word').attr('data-answers-count')) + 1);
    
    $('span.training-current-progress-number').html(data);
    $('.training-current-progress-bar').css('width', data*100/totalProgress + "%");
    $(".training-current-progress-bar").css("transition", "0.4s cubic-bezier(0.75, 0.11, 1, 1)");
    
    var finish = words.length == 0 ? 1 : 0;
    $.ajax({
        url: '/site/set-points',
        method: 'POST',
        data: { 
            answerId: answerId,
            rightAnswer: isRight,
            finish: finish
        },
        dataType: 'json',
        success: function(data) {
            if(finish) {
                setTimeout(function() {
                    //setTimeout(function() { window.location.href = '/site/tasks'; }, 500);
                    $('.main').find('.training').slideUp(450);
                    setTimeout(function() { 
                        $('.main').html(data.result); 
                        $('.main').find('.result').fadeIn(450);
                        $('.cursorTooltip').html(data.nextLevelIn);
                        tooltipHover();
                        setTimeout(makeResult, 300);
                        $(document).find('.header-progress').html(data.headerProgress);
                        setTimeout(setLevelProgress, 200);
                        toggleExperience(false);
                        setTimeout(function() { toggleLevel(false) }, 900);}, 
                    450);
                    return;
                },
                500);
            }
        }
    });
    
    if(words.length == 0) {
        return false;
    }
    
    var answerId = Math.floor(Math.random() * 5);
    $('.training-inner').find('h3.training-word').attr('data-answer-item', words[0]['main_id']);
    $('.training-inner').find('h3.training-word').attr('data-answers-count', data);
    $('.training-inner').find('h3.training-word').html(words[0]['main_word']);
    
    var liAnswer = $('.training-inner').find("div.training-translate-list ul li:first-child");
    for(var i = 0; i < 5; i++) {
        if(i == answerId) {
            liAnswer.attr('data-answer-item', words[0]['translate_id']);
            liAnswer.find('b').html(words[0]['translate_word']);
        } else if(i == 0) {
            liAnswer.attr('data-answer-item', words[answerId]['translate_id']);
            liAnswer.find('b').html(words[answerId]['translate_word']);
        } else {
            liAnswer.attr('data-answer-item', words[i]['translate_id']);
            liAnswer.find('b').html(words[i]['translate_word']);
        }
        liAnswer = liAnswer.next();
    }
    $("li.last a b").html('Не знаю :(');
    
    $('.answer-success').removeClass('answer-success');
    $('.answer-fail').removeClass('answer-fail');
    $('.answer-fail').removeClass('answer-skipped');
    
    words = words.slice(5);
    
    $('.training-inner').find('li.last a').attr('id', 'skip-item');
    $('.training-inner').find('.training-timer-text').html(5);
    idIntervalRound = roundTimeOut($('.training-inner').find('.training-round'));
    idInterval = intervalTimeOut();
    
    console.timeEnd('trigger');
}

function intervalTimeOut() {
    var id = setInterval(function() { timeOut(id) }, 1000);
    return id;
}

function roundTimeOut(elem) {
    var $$ = {
            radius  :     40, // радиус окружности 
            speed   :     5 // скорость/задержка ( в js это мс, например 10 мс = 100 кадров в секунду)
    }
    var f = 3;
    var s = 1.811 * Math.PI / 180; //Вычислим угол
    var id = setInterval(function() { // функция движения
                var time = $('.training-inner').find('.training-timer-text').html();
                if(time == 0) {
                    return id;
                }
            f += s; // приращение аргумента
              elem.css('right', 29 + $$.radius * Math.sin(f)  + 'px'); // меняем координаты элемента, подобно тому как мы это делали в школе в декартовой системе координат. Правда, в данном случае используется полярная система координат, изменяя угол
              elem.css('top', 40 + $$.radius * Math.cos(f) + 'px');
    }, $$.speed)
    return id;
}

function timeOut(id, id2) {
    var time = ($(window).width() <= 768) ? $('.training-inner .mobile').find('.training-timer-text').html() : $('.training-inner .no-mobile').find('.training-timer-text').html();
    if(time == 0) {
        clearInterval(id);
        clearInterval(id2);
        $('.training-inner').find('#skip-item').click();
        return;
    }
    $('.training-timer-text').html(time - 1);
}