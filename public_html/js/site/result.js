jQuery(document).ready(function () {
    $(document).bind('keyup', function(event) {
        if(event.keyCode == 13 && $('.result').length) {
            location.reload();
        }
    });
});

function tooltipHover() {
    $(document).find('.stats-progress').hover(function() {
        tooltip(this, $('.cursorTooltip').html());
    }, function() {
        hide_info(this);
    });
}

function makeResult() {
    var R = $('.svg-stats-progress').attr('r'),
        L = 2*Math.PI*R,
        totalExperience = $('.profile-experience-progress').data('experience-total'),
        progressExperience = $('.profile-experience-progress').data('experience-progress'),
        sector = L/totalExperience,
        angleProgress = progressExperience*sector,
        progressSVG = (Math.PI*R*angleProgress)/180,
        sectorProgress = progressSVG*360/(2*Math.PI*R);
    $('.svg-stats-progress').attr('stroke-dasharray', sectorProgress + ', 999');
    
    var decimal_places = 0;
    var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;

    $('#target-progress-experience').animateNumber(
    {
        number: progressExperience * decimal_factor,
        numberStep: function(now, tween) {
            var floored_number = Math.floor(now) / decimal_factor,
                    target = $(tween.elem);
            if (decimal_places > 0) {
                floored_number = floored_number.toFixed(decimal_places);
            }
            target.text(floored_number);
        }
    },
    700
    );
}

var d = document;
var offsetfromcursorY=30 // y offset of tooltip
var ie=d.all && !window.opera;
var ns6=d.getElementById && !d.all;
var tipobj,op;
 
function tooltip(el,txt) {
    tipobj=d.getElementById('cursorTooltip');
    tipobj.innerHTML = txt;
    op = 0.1;  
    tipobj.style.opacity = op;
    tipobj.style.visibility="visible";
    el.onmousemove=positiontip;
    appear();
}
 
function hide_info(el) {
    d.getElementById('cursorTooltip').style.visibility='hidden';
    el.onmousemove='';
}
 
function ietruebody(){
    return (d.compatMode && d.compatMode!="BackCompat")? d.documentElement : d.body
}
 
function positiontip(e) {
    var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
    var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
    var winwidth=ie? ietruebody().clientWidth : window.innerWidth-20;
    var winheight=ie? ietruebody().clientHeight : window.innerHeight-20;

    var rightedge=ie? winwidth-event.clientX : winwidth-e.clientX;
    var bottomedge=ie? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY;

    if (rightedge < tipobj.offsetWidth)  tipobj.style.left=curX-15-tipobj.offsetWidth+"px";
    else tipobj.style.left=curX-15+"px";

    if (bottomedge < tipobj.offsetHeight) tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px";
    else tipobj.style.top=curY+offsetfromcursorY+"px";
}
 
function appear() {
    if(op < 1) {
        op += 0.1;
        tipobj.style.opacity = op;
        tipobj.style.filter = 'alpha(opacity='+op*100+')';
        t = setTimeout('appear()', 30);
    }
}