$(document).ready(function() {
    $(".no-mobile .index-item-info").hover( function () {
        $(this).parent().find(".index-item-button").removeClass("index-item-button-hover");
        $(this).parent().parent().find(".index-item-detailed-info").addClass("index-item-detailed-info-hover");
    }, function() {
        if(!$(".no-mobile .index-item-detailed-info:hover").length) {
            $(this).parent().parent().find(".index-item-detailed-info").removeClass("index-item-detailed-info-hover");
        }
    });
    
    $(".no-mobile .index-item").hover( function () {
        $(this).find(".index-item-button").addClass("index-item-button-hover");
    }, function () {
        $(this).find(".index-item-button").removeClass("index-item-button-hover");
    });
    
    $(".no-mobile .index-item-detailed-info").on('mouseout', function() {
        $(this).parent().find(".index-item-description .index-item-button").addClass("index-item-button-hover");
        $(this).removeClass("index-item-detailed-info-hover");
    });
    
    $(".button31").click(function() {
        setTimeout(function() { $(".button31").blur() }, 500);
    });
    
    $(".index-item-repeat-it").click(function() {
        var category = $(".index-item-select-words select").val() == null ? '' : '/' + $(".index-item-select-words select").val();
        window.location.href = "/site/repeat/it" + category;
    });
    
    $(".index-item-repeat-ru").click(function() {
        var category = $(".index-item-select-words select").val() == null ? '' :'/' + $(".index-item-select-words select").val();
        window.location.href = "/site/repeat/ru" + category;
    });
    
    $(".index-item-constructor").click(function() {
        var category = $(".index-item-select-words select").val() == null ? '' : '/' + $(".index-item-select-words select").val();
        window.location.href = "/site/constructor" + category;
    });
    
});

function limitState(value) {
    $.ajax({
        url: '/site/set-limit',
        method: 'POST',
        data: { 
            value: value
        }
    });
}
