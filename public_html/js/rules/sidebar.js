$(document).ready(function() {
    $('.sidebar').hover( function() { 
        $(this).addClass('sidebar__hover'); 
        //$('.main').attr('style', 'background-color: #b7b4b4; transition: .3s linear');
        $('.main').append('<div class="overlay" style="position: absolute; background-color: rgba(0,0,0,1); opacity: 0.3; height: 100%; width: 100%; top: 0; left: 0; animation: fadeInFromNone .3s linear"></div>')
    }, function() { 
        $(this).removeClass('sidebar__hover');
        //$('.main').attr('style', 'transition: .3s linear');
        $('.main').find('.overlay').attr('style', "position: absolute; background-color: rgba(0,0,0,1); opacity: 0; height: 100%; width: 100%; top: 0; left: 0; animation: fadeOutFromNone .2s linear");
        setTimeout(function() { $('.main').find('.overlay').remove(); }, 200);
    });
});

