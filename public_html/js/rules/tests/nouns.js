var nextTest = 0;
var testNum = Math.round(Math.random() * (2 - 1)) + 1;
jQuery(document).ready(function() {
    $.ajax({
        url: '/rule/nouns-generate',
        method: 'POST',
        data: {test: nextTest, testNum: testNum},
        dataType: 'json',
        success: function(result) {
            if(result.status) {
                $('.test-content').html(result.data);
                $('.test-content').fadeIn(1);
                nextTest++;
                $('.test').find('.test-answers ol li span').bind('click', answer);
            } else {
                console.log('No file!');
            }
        }
    });
    
    $(document).bind('keyup', function(event) {
        if(event.keyCode == 49) {
             $('.test .test-answers ol li:nth-child(1) span').click();
        }
        if(event.keyCode == 50) {
             $('.test .test-answers ol li:nth-child(2) span').click();
        }
        if(event.keyCode == 51) {
             $('.test .test-answers ol li:nth-child(3) span').click();
        }
    });
});

function answer() {
    $('.test').find('.test-answers ol li span').unbind('click', answer);
    $('.test').find('.test-answer .variant').html($(this).html());
    if(typeof $(this).parent().data('answer') == 'undefined') {
        $('.test').find('.test-answer .variant').addClass('variant-mistake');
        setAnswerStatus('wrong');
    } else {
        setAnswerStatus('correct');
    }
    $(this).css('transition', '.5s linear');
    $('.test').find('.test-answers ol, .test-question').css('transition', '.3s linear');
    $('.test').find('.test-answers ol, .test-question').css('opacity', 0);
    $(this).css('left', '50px');
    $('.test').find('.test-answers ol').css('visibility', 'hidden');
    setTimeout(function() {
        $('.test').find('.test-answers ol').css('display', 'none');
        $('.test').find('.test-answer').css('transition', '.3s linear');
        $('.test').find('.test-answer').css('opacity', 1);
        $('.test').find('.test-answer').css('visibility', 'visible');
        setTimeout(function() {
            $('.test').find('.test-answer').css('transition', '.3s linear');
            $('.test').find('.test-answer').css('opacity', 0);
            $('.test').find('.test-answer').css('visibility', 'hidden');
            $('.test').find('.test-answers ol').css('display', 'none');
            setTimeout(function() {
                $.ajax({
                    url: '/rule/nouns-generate',
                    method: 'POST',
                    data: {test: nextTest, testNum: testNum},
                    dataType: 'json',
                    success: function(result) {
                        if(result.end) {
                            $.ajax({
                                url: '/rule/after-test',
                                method: 'POST',
                                data: {experience: $('.test-task-status .correct').length},
                                dataType: 'json',
                                success: function(data) {
                                    $(document).find('.header-progress').html(data.headerProgress);
                                    setTimeout(setLevelProgress, 200);
                                    $('.test-result').modal('show');
                                    makeResultTest();
                                }
                            });
                        }
                        if(result.status) {
                            $('.test-content').hide();
                            $('.test-content').html(result.data);
                            $('.test-content').fadeIn(500);
                            nextTest++;
                            $('.test').find('.test-answers ol li span').bind('click', answer);
                        } else {
                            console.log('No file!');
                        }
                    }
                });
            }, 300);
        }, 2500);
    }, 400);
}

function setAnswerStatus(statusAnswer) {
        $('.next').find('i').addClass(statusAnswer);
        var index = $('.next').index();
        $('.test-task-status:eq(' + (index + 1) + ')').addClass('next');
        $('.test-task-status:eq(' + index + ')').removeClass('next');
}
