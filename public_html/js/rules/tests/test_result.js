jQuery(document).ready(function () {
});

function makeResultTest() {
    var R = $('.test-svg-stats-progress').attr('r'),
        L = 2*Math.PI*R,
        totalExperience = $('.profile-experience-progress').data('experience-total'),
        progressExperience = $('.profile-experience-progress').data('experience-progress'),
        sector = L/totalExperience,
        angleProgress = progressExperience*sector,
        progressSVG = (Math.PI*R*angleProgress)/180,
        sectorProgress = progressSVG*360/(2*Math.PI*R);
    $('.test-svg-stats-progress').attr('stroke-dasharray', sectorProgress + ', 999');
    
    var decimal_places = 0;
    var decimal_factor = decimal_places === 0 ? 1 : decimal_places * 10;

    $('#test-target-progress-experience').animateNumber(
    {
        number: progressExperience * decimal_factor,
        numberStep: function(now, tween) {
            var floored_number = Math.floor(now) / decimal_factor,
                    target = $(tween.elem);
            if (decimal_places > 0) {
                floored_number = floored_number.toFixed(decimal_places);
            }
            target.text(floored_number);
        }
    },
    700
    );
}