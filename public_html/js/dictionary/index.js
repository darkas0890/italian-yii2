$(document).ready(function() {
    $('.dictionary').on('click', '.dictionary-category', function() {
        var element = $(this);
        var categoryId = $(this).data('category-id');
        var categoryTitle = $(this).html();
        $.ajax({
            url: '/dictionary/generate-words',
            method: 'POST',
            dataType: 'json',
            data: {categoryId: categoryId},
            success: function(data) {
                $('.dictionary-words').html(data.data);
                $('[data-toggle="popover"]').popover();
                $('.dictionary-words-head h3').html(categoryTitle);
                $('.dictionary-category').removeAttr('style');
                element.css('background', '#efefef');
                element.addClass('cactive');
                if(data.isCustom)
                    $('.custom-items-tools').show();
                else
                    $('.custom-items-tools').hide();
            }
        });
    });
    
    //Double click on dictionary italian word
    $('.dictionary-words').on('dblclick', '.dictionary-item-word-it.superadmin', function (event) {
        if ($(event.target).closest(".dictionary-item-word-it input").length) return;
        var itWord = $(this).html();
        $(this).html('');
        $(this).append('<input type="text" class="dictionary-word-input-it">');
        $('.dictionary-word-input-it').focus();
        $('.dictionary-word-input-it').val($.trim(itWord));
    });
    
    //Double click on dictionary russian word
    $('.dictionary-words').on('dblclick', '.dictionary-item-word-ru.superadmin', function (event) {
        if ($(event.target).closest(".dictionary-item-word-ru input").length) return;
        var ruWord = $(this).html();
        $(this).html('');
        $(this).append('<input type="text" class="dictionary-word-input-ru">');
        $('.dictionary-word-input-ru').focus();
        $('.dictionary-word-input-ru').val($.trim(ruWord));
    });
    
    //Double click on dictionary category
    $('.dictionary-words').on('dblclick', '.dictionary-item-category.superadmin', function (event) {
        if ($(event.target).closest(".dictionary-item-category select").length) return;
        var category = $(this).html();
        var categoryId = $(this).data('category-id');
        var categoryContent = $(this);
        $(this).html('');
        $.ajax({
            url: '/dictionary/get-categories',
            method: 'POST',
            dataType: 'json',
            success: function(data) {
                categoryContent.append('<select class="dictionary-word-input-category"></select>');
                $.each( data.categories, function( index, value ){
                    if(categoryId == value.id) {
                        $(".dictionary-word-input-category").append($("<option></option>")
                                .attr("value", value.id)
                                .attr("class", "dictionary-category-select")
                                .attr("selected", "seleted")
                                .text(value.category));
                    } else {
                        $(".dictionary-word-input-category").append($("<option></option>")
                                .attr("value", value.id)
                                .attr("class", "dictionary-category-select")
                                .text(value.category));
                    }
                });
            }
        });
    });
    
    //Check if there is an input in Dictionary and click was out of it, then remove it
    $(document).click(function(event) {
        if($(event.target).closest(".dictionary-item-word-it input").length) return;
        if($(".dictionary-item-word-it input").length) {
            var li = $('.dictionary-words').find(".dictionary-item-word-it input");
            var itWordId = li.parent().data('it-id');
            var itWord = li.val();
            li = li.parent();
            li.html(itWord);
            $.ajax({
                url: '/dictionary/update-it-word',
                type: 'POST',
                data: { 
                    itWord: itWord,
                    itWordId: itWordId },
                dataType: 'json',
                success: function(data) {
                    //$('.dictionary-words').html(data.html);
                }
            });
        }
    });
    
    $(document).click(function(event) {
        if ($(event.target).closest(".dictionary-item-word-ru input").length) return;
        if($(".dictionary-item-word-ru input").length) {
            var li = $('.dictionary-words').find(".dictionary-item-word-ru input");
            var ruWordId = li.parent().data('ru-id');
            var ruWord = li.val();
            li = li.parent();
            li.html(ruWord);
            $.ajax({
                url: '/dictionary/update-ru-word',
                type: 'POST',
                data: { 
                    ruWord: ruWord,
                    ruWordId: ruWordId },
                dataType: 'json',
                success: function(data) {
                    //$('.dictionary-words').html(data.html);
                }
            });
        }
    });
    
    $(document).click(function(event) {
        if ($(event.target).closest(".dictionary-item-category select").length) return;
        if($(".dictionary-item-category select").length) {
            var li = $('.dictionary-words').find(".dictionary-item-category select");
            var itWordId = $.trim(li.closest('.dictionary-item').find('.dictionary-item-word-it').data('it-id'));
            var categoryWord = li.find("option:selected").text();
            var categoryId = li.find("option:selected").val();
            li = li.parent();
            li.html(categoryWord);
            $.ajax({
                url: '/dictionary/set-category',
                type: 'POST',
                data: { 
                    categoryId: categoryId,
                    itWordId: itWordId },
                dataType: 'json',
                success: function(data) {
                    $('.dictionary-words').html(data.html);
                    $('[data-toggle="popover"]').popover();
                }
            });
        }
    });
    
    $('.dictionary').on('keyup', '#dictionary-filter', function() {
        var obj = jQuery(this),
                val = jQuery.trim(obj.val()),
                listAllWords = jQuery('.dictionary').find('.dictionary-words-container .dictionary-item');

        obj.data('filter-process', true);

        if (val === '') {
            listAllWords.fadeIn(100, function(){
                obj.data('filter-process', null);
                return true;
            });
        }

        regexp = new RegExp(val, 'i');

        listAllWords.each(function(){
            var curObj = jQuery(this),
                    curVal = jQuery.trim(curObj.html());
            if (!regexp.test(curVal)) {
                curObj.fadeOut(100);
            } else {
                curObj.fadeIn(100);
            }

        });
        obj.data('filter-process', null);
        return true;
    });
    
    $('.dictionary').on('change', '.dictionary-item-select', function () {
        if ($('.dictionary-item-select:checked').length > 0) {
            $('.selected-items-tools').show();
        } else {
            $('.selected-items-tools').hide();
        }
    });
    
    $('.dictionary').on('click', '.addToCustomCategory', function() {
        if(confirm('Добавить?')) {
            var itWordId = [];
            var ruWordId = [];
            $('.dictionary-item-select:checked').each(function(index, el) {
                itWordId.push($(el).parent().find(".dictionary-item-word-it").data('it-id'));
                ruWordId.push($(el).parent().find(".dictionary-item-word-ru").data('ru-id'));
            });
            $.ajax({
                url: '/dictionary/set-custom-category',
                type: 'POST',
                data: {
                    itWords: itWordId,
                    ruWords: ruWordId
                },
                dataType: 'json',
                success: function(data) {
                }
            });
        }
    });
    
    $('.dictionary').on('click', '.cleanCustomCategory', function() {
        if(confirm('Желаете очистить набор?')) {
            $.ajax({
                url: '/dictionary/clean-custom-category',
                type: 'POST',
                complete: function() {
                    $('.dictionary').find('.cactive').click();
                }
            });
        }
    });
});