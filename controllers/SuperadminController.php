<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SuperadminController extends Controller
{
    public function actionIndex() {
        $selectedCategory = Yii::app()->request->getParam('cid', false);
        $modelCategory = Category::model()->findAll();
            
        $this->render('index', array('modelCategory' => $modelCategory, 'selectedCategory' => $selectedCategory));
    }
    
    public function actionCreate() {
        $itWord = Yii::app()->request->getParam('ItInput', false);
        $ruWord = Yii::app()->request->getParam('RuInput', false);
        $category = Yii::app()->request->getParam('Category', false);

        $modelRu = new RuLanguage;
        $modelIt = new ItLanguage;

        $modelRu->word = trim($ruWord);
        $modelIt->word = trim($itWord);

        if($modelRu->validate() && $modelIt->validate() && $category) {
            $modelRuIt = new RuItLanguages;
            $modelRu->save(false);
            $modelIt->save(false);

            $modelRuIt->id_ru = $modelRu->id;
            $modelRuIt->id_it = $modelIt->id;
            $modelRuIt->id_category = $category;
            $modelRuIt->save(false);
        }

        Yii::app()->controller->redirect(array('/superadmin/index', 'cid' => $category));
    }
}