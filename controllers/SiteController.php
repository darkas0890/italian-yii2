<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegistrationForm;
use app\models\Category;
use app\models\User;
use app\models\ItLanguage;
use app\models\RuItLanguages;
use app\models\RuLanguage;
use app\models\Activity;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\Controller;
use yii\db\Query;

class SiteController extends Controller
{
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function beforeAction($action) {
        
        //actionRepeat, actionConstructor
        if(in_array("{$action->id}", ['repeat', 'constructor'])) {
            Yii::$app->session->set('temp', [
                'temp_correct_answers' => 0,
                'temp_correct_answer_id' => [],
                'temp_current_words' => [],
                ]);
            
            $cache = Yii::$app->cache;
            $data = $cache->get('translateModel');
            if($data)
                $cache->delete('translateModel');
            
            $data = $cache->get('mainModel');
            if($data)
                $cache->delete('mainModel');
        }
        
        return parent::beforeAction($action);
    }
    
    public function actionIndex() {
        $this->layout = 'landing';
        return $this->render('index');
    }
    
    public function actionAuthorization() {
        $form = Yii::$app->request->post('ajax', false);
        if($form) {
            if($form == 'login-form') {
                $model = new LoginForm();
                $model->email = Yii::$app->request->post('LoginForm')['email'];
                $model->password = Yii::$app->request->post('LoginForm')['password'];
                if ($model->login()) {
                    return $this->redirect(Url::to('site/home'));
                }
            } elseif($form == 'registration-form') {
                $model = new RegistrationForm();
                $model->firstname = Yii::$app->request->post('RegistrationForm')['firstname'];
                $model->sex = Yii::$app->request->post('RegistrationForm')['sex'];
                $model->email = Yii::$app->request->post('RegistrationForm')['email'];
                $model->password = Yii::$app->request->post('RegistrationForm')['password'];
                if($model->registrate()) {
                    $login = new LoginForm(['email' => $model->email, 'password' => $model->password]);
                    if($login->login())
                        return $this->redirect(Url::to('site/home'));
                }
            }
        }
        $this->layout = 'landing';
        return $this->render('authorization');
    }
    
    public function actionHome() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('home');
    }
    
    public function actionRepeat($language) {
        $language = Yii::$app->request->get('language', false);
        $category = Yii::$app->request->get('category', false);
        $cache = Yii::$app->cache;
        if(!$language)
            throw new HttpException(500,Yii::t('yii','Language is not selected.'));
        
        $query = new Query();
        $query2 = new Query();
        $queryIds = new Query();
        
        $trainingLimit = Yii::$app->session->get('user.trainingLimit');
        
        $queryIds->from('ru_it_languages as main');
        if($language == 'ru') {
            $queryIds->select('main.id_ru as id');
        } elseif($language == 'it') {
            $queryIds->select('main.id_it as id');
        }
        if($category) {
            $queryIds->andWhere(['main.id_category' => $category]);
        }
        $ids = $queryIds->orderBy('RAND()')->limit($trainingLimit)->all();
        if(count($ids) < $trainingLimit) {
            $trainingLimit = count($ids);
        }
        
        if($language == 'ru') {
            $query->from('ru_language as main')
                    ->innerJoin('ru_it_languages as ru_it', 'ru_it.id_ru = main.id')
                    ->innerJoin('it_language as translate', 'translate.id = ru_it.id_it');
            
            $query2->from('ru_language as main')
                    ->innerJoin('ru_it_languages as ru_it', 'ru_it.id_ru = main.id')
                    ->innerJoin('it_language as translate', 'translate.id = ru_it.id_it');
        } elseif($language == 'it') {
            $query->from('it_language as main')
                    ->innerJoin('ru_it_languages as ru_it', 'ru_it.id_it = main.id')
                    ->innerJoin('ru_language as translate', 'translate.id = ru_it.id_ru');
            
            $query2->from('it_language as main')
                    ->innerJoin('ru_it_languages as ru_it', 'ru_it.id_it = main.id')
                    ->innerJoin('ru_language as translate', 'translate.id = ru_it.id_ru');
            
        }
        if($category) {
            $query->andWhere(['ru_it.id_category' => $category]);
        }
        
        //SELECT CONCAT(UPPER(LEFT(main.word,1)), LOWER(SUBSTRING(main.word,2,LENGTH(main.word))));
        //UPDATE it_language SET word = CONCAT(UPPER(LEFT(word,1)), LOWER(SUBSTRING(word,2,LENGTH(word))))
        $query2->select('main.id as main_id, main.word as main_word, translate.id as translate_id, translate.word as translate_word')
                    ->orderBy('RAND()')
                    ->limit(4);
        $query->select('main.id as main_id, main.word as main_word, translate.id as translate_id, translate.word as translate_word')
                ->union($query2);
        $words = [];
        
        foreach($ids as $id) {
            $query2->where(['!=', 'main.id', $id['id']]);
            $query->where(['main.id' => $id['id']]);
            $temp = $query->all();
            $words = array_merge($words, $temp);
        }
        
        return $this->render('repeat', ['words' => $words, 'trainingLimit' => $trainingLimit]);
    }
    
    public function actionSetPoints() {
        $startTime = microtime(1);
        $answerId = Yii::$app->request->post('answerId', 0);
        $isFinish = Yii::$app->request->post('finish', false);
        $correctAnswer = Yii::$app->request->post('rightAnswer', false);
        $correctAnswer = $correctAnswer == "true" ? true : false;

        $temp = Yii::$app->session->get('temp');
        array_push($temp['temp_current_words'], $answerId);
        if($correctAnswer) {
            array_push($temp['temp_correct_answer_id'], $answerId);
            $temp['temp_correct_answers'] = $temp['temp_correct_answers'] + 1;
        }
        Yii::$app->session->set('temp', $temp);
        
        if($isFinish) {
            $modelActivity = Activity::find()
                    ->where(['date' => date('Y-m-d'), 'user_id' => Yii::$app->user->identity->id])
                    ->one();
            $modelUser = User::findIdentity(Yii::$app->user->identity->id);
            $modelUser->experience += $temp['temp_correct_answers'];
            $modelUser->total_experience += $temp['temp_correct_answers'];
            
            $pointsText = RuLanguage::setPointsWord($temp['temp_correct_answers']);
            $gainedExperienceText = '+' . $temp['temp_correct_answers'] . ' ' . $pointsText;
            $gainedLevel = false;
            $gainedLevelText = '';
            if($modelUser->experience >= $modelUser->levels->experience) {
                $gainedLevel = true;
                $diffExperience = $modelUser->experience - $modelUser->levels->experience;
                $modelUser->experience = $diffExperience;
                $modelUser->level += 1;
                if(empty(json_decode($modelActivity->gain_achievements)->{"level"})) {
                    $modelActivity->gain_achievements = json_encode(['level' => [['Reached level ', $modelUser->level]]]);
                } else {
                    $levelAchieves = json_decode($modelActivity->gain_achievements);
                    $levelAchieves->{"level"}[] = ['Reached level ', $modelUser->level];
                    $modelActivity->gain_achievements = json_encode($levelAchieves);
                }
                $gainedLevelText = \Yii::t('main', 'Level up!');
            }
            if(empty($modelActivity)) {
                $modelActivity = new Activity;
                $modelActivity->date = date('Y-m-d');
                $modelActivity->user_id = Yii::$app->user->identity->id;
                $modelActivity->gain_experience = $temp['temp_correct_answers'];
            } else {
                $modelActivity->gain_experience += $temp['temp_correct_answers'];
            }
            $modelUser->save(false);
            $modelActivity->save(false);
            $headerProgress = $this->renderPartial('block/header_progress', [
                'userModel' => $modelUser
                ]);
            $result = $this->renderPartial('block/result', [
                'words' => $temp['temp_current_words'], 
                'rightAnswers' => $temp['temp_correct_answer_id'], 
                'gainedExperienceText' => $gainedExperienceText,
                'gainedLevel' => $gainedLevel,
                'gainedLevelText' => $gainedLevelText,
                    ]);
            echo json_encode([
                'result' => $result, 
                'headerProgress' => $headerProgress, 
                'progress' => $temp['temp_correct_answers'],
                'nextLevelIn' => Activity::toolTipProgress()
                    ]);
            die();
        }

        $endTime = microtime(1);
        $diff = $endTime - $startTime;
        echo json_encode([
            'diff' => $diff,
            'answers' => $temp['temp_correct_answers'],
            'words' => $temp['temp_correct_answer_id'],
                ]);
    }
    
    public function actionConstructor() {
        $category = Yii::$app->request->get('category', false);
        
        $word = ItLanguage::find()
                ->innerJoin('ru_it_languages as rt_lang', 'rt_lang.id_it = id')
                ->limit(1)
                ->orderBy('RAND()');
        
        $trainingLimit = Yii::$app->session->get('user.trainingLimit');
        if($category) {
            $word->where(['rt_lang.id_category' => $category]);
            $limitCount = RuItLanguages::find()
                    ->limit($trainingLimit)
                    ->where(['id_category' => $category])->all();
            if(count($limitCount) < $trainingLimit) {
                $trainingLimit = count($limitCount);
            }
        }
        $word = $word->one();
        
        return $this->render('constructor', ['word' => $word, 'trainingLimit' => $trainingLimit]);
    }

    public function actionGenerateNewWord() {
        //POST
        $category = Yii::$app->request->post('category', false);
        $answersCount = Yii::$app->request->post('answersCount', 0);
        $answerId = Yii::$app->request->post('answerId', 0);
        $correctAnswer = Yii::$app->request->post('rightAnswer', false);
        $category = $category == "false" ? false : $category;

        //SESSION temp data
        $trainingLimit = Yii::$app->session->get('user.trainingLimit');
        if($category) {
            $limitCount = RuItLanguages::find()
                    ->limit($trainingLimit)
                    ->where(['id_category' => $category])->all();
            if(count($limitCount) < $trainingLimit) {
                $trainingLimit = count($limitCount);
            }
        }
        $temp = Yii::$app->session->get('temp');
        $correctAnswers = $temp['temp_correct_answers'];
        $correctAnswerIds = $temp['temp_correct_answer_id'];
        $currentWords = $temp['temp_current_words'];
        
        array_push($currentWords, $answerId);
        $temp['temp_current_words'] = $currentWords;
        
        if($correctAnswer) {
            array_push($correctAnswerIds, $answerId);
            $temp['temp_correct_answer_id'] = $correctAnswerIds;
            $correctAnswers++;
            $temp['temp_correct_answers'] = $correctAnswers;
        }
        Yii::$app->session->set('temp', $temp);
        
        if($answersCount >= $trainingLimit) {
            Yii::$app->session->remove('temp');
            $modelUser = User::findIdentity(Yii::$app->user->identity->id);
            $userProgress = $modelUser->experience;
            $modelUser->experience += $correctAnswers;
            $modelUser->total_experience += $correctAnswers;

            $modelActivity = Activity::find()
                    ->where(['date' => date('Y-m-d'), 'user_id' => Yii::$app->user->identity->id])
                    ->one();
            if(empty($modelActivity)) {
                $modelActivity = new Activity;
                $modelActivity->date = date('Y-m-d');
                $modelActivity->user_id = Yii::$app->user->identity->id;
                $modelActivity->gain_experience = $correctAnswers;
            } else {
                $modelActivity->gain_experience += $correctAnswers;
            }

            $pointsText = RuLanguage::setPointsWord($correctAnswers);
            $gainedExperienceText = '+' . $correctAnswers . ' ' . $pointsText;
            $gainedLevel = false;
            $gainedLevelText = '';
            $modelUser->correct_answers = 0;
            $modelUser->current_words = null;
            if($modelUser->experience >= $modelUser->levels->experience) {
                $gainedLevel = true;
                $diffExperience = $modelUser->experience - $modelUser->levels->experience;
                $modelUser->experience = $diffExperience;
                $modelUser->level += 1;
                if(empty(json_decode($modelActivity->gain_achievements)->{"level"})) {
                    $modelActivity->gain_achievements = json_encode(['level' => [['Reached level ', $modelUser->level]]]);
                } else {
                    $levelAchieves = json_decode($modelActivity->gain_achievements);
                    $levelAchieves->{"level"}[] = ['Reached level ', $modelUser->level];
                    $modelActivity->gain_achievements = json_encode($levelAchieves);
                }
                $gainedLevelText = \Yii::t('main', 'Level up!');
            }
            $modelUser->save(false);
            $modelActivity->save(false);
            $action = $category ? '/site/constructor/' . $category : '/site/constructor';
            $headerProgress = $this->renderPartial('block/header_progress', [
                'userModel' => $modelUser
                ]);
            $result = $this->renderPartial('block/result', [
                'words' => $currentWords, 
                'rightAnswers' => $correctAnswerIds, 
                'gainedExperienceText' => $gainedExperienceText,
                'gainedLevel' => $gainedLevel,
                'gainedLevelText' => $gainedLevelText,
                'action' => $action]);
            echo json_encode(['end' => true, 
                'result' => $result, 
                'headerProgress' => $headerProgress, 
                'progress' => $answersCount,
                'nextLevelIn' => Activity::toolTipProgress()
                    ]);
            die();
        }
        $word = ItLanguage::find()
                ->innerJoin('ru_it_languages as rt_lang', 'rt_lang.id_it = id')
                ->where(["NOT IN", "rt_lang.id_it", $currentWords])
                ->limit(1)
                ->orderBy('RAND()');
        
        if($category) {
            $word->andWhere(['rt_lang.id_category' => $category]);
        }
        $word = $word->one();

        $data = $this->renderPartial('block/constructor_word', [
            'word' => $word,
            'answersCount' => $answersCount
                ]);
        
        echo json_encode(['html' => $data, 'progress' => $answersCount]);
    }
    
    public function actionSetLimit() {
        $value = Yii::$app->request->post('value', 10);
        Yii::$app->session->set('user.trainingLimit', $value);
    }
    
    public function actionTasks() {
        $selectedCategory = Yii::$app->request->post('cid', false);
        $modelCategory = Category::find()->where(['user_id' => [Category::DEFAULT_CATEGORY, Yii::$app->user->identity->id]])->all();

        return $this->render('tasks', [
            'modelCategory' => $modelCategory,
            'selectedCategory' => $selectedCategory
                ]);
    }
    
    public function actionProfile() {
        $modelUser = User::getUser();

        $modelActivity = Activity::find()
                ->where(['user_id' => Yii::$app->user->identity->id])
                ->limit(7)
                ->orderBy('date DESC')
                ->all();
        //Получаем список стран из вк
        $lang = 3; // english
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" . // Вероятно этот параметр ни на что не влияет
                "Cookie: remixlang=$lang\r\n"
            )
        );
        $methodUrl = 'http://api.vk.com/method/database.getCountries?v=5.5&need_all=1&offset=0&count=1000';
        $streamContext = stream_context_create($headerOptions);
        $json = file_get_contents($methodUrl, false, $streamContext);
        $arr = json_decode($json, true);

        return $this->render('profile', [
            'user' => $modelUser,
            'modelActivity' => $modelActivity,
            'countries' => $arr['response']['items']
                ]);
    }
        
    public function actionLang($lang) {
        $_SESSION['lang'] = $lang;
        $user = User::getUser();
        $user->language = $lang;
        $user->save(false);
        $back = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : array('/');
        return $this->redirect($back);
    }
    
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            $statusCode = $exception->getCode();
            $name = $exception->getName();
            $message = $exception->getMessage();
            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
                    ]);
        }
    }
}
