<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\RuItLanguages;
use app\models\RuLanguage;
use app\models\ItLanguage;
use app\models\User;
use yii\db\Query;
use yii\web\Controller;

class DictionaryController extends Controller
{
    public function actionIndex() {
        $modelCategory = Category::find()->where(['user_id' => [Category::DEFAULT_CATEGORY, Yii::$app->user->identity->id]])->all();
        $query = new Query;
        $countWords = $query->from('ru_it_languages as r')
                ->innerJoin('category as c', 'r.id_category = c.id')
                ->where(['c.user_id' => [Category::DEFAULT_CATEGORY, Yii::$app->user->identity->id]])->count();
        return $this->render('index', array('modelCategory' => $modelCategory, 'countWords' => $countWords, 'isAllUsers' => true));
    }
    
    public function actionGenerateWords() {
        $categoryId = Yii::$app->request->post('categoryId', false);
        if(!$categoryId) {
            $modelCategory = Category::find()->where(['user_id' => [Category::DEFAULT_CATEGORY, Yii::$app->user->identity->id]])->all();
            $isAllUsers = true;
        } else {
            $modelCategory = Category::find()->where(['id' => $categoryId])->all();
            $isAllUsers = false;
        }
        
        $isCustom = Yii::$app->user->identity->id == $modelCategory[0]->user_id ? true : false;
        $data = $this->renderPartial('block/words', [
            'modelCategory' => $modelCategory,
            'isAllUsers' => $isAllUsers
                ]);
        
        echo json_encode(['data' => $data, 'isCustom' => $isCustom]);
    }
    
    public function actionGetCategories() {
        $modelCategory = Category::find()->all();
        $categories = [];
        $i = 0;
        foreach($modelCategory as $category) {
            $categories[$i]['id'] = $category->id;
            $categories[$i]['category'] = $category->category;
            $i++;
        }
        echo json_encode(['categories' => $categories]);
    }
    
    public function actionSetCategory() {
        if(Yii::$app->user->identity->role != User::ROLE_SUPERADMIN)
            return;
        $categoryId = (int) Yii::$app->request->post('categoryId', 0);
        $idWordId = Yii::$app->request->post('itWordId', 0);
        $languages = RuItLanguages::model()->updateAll(array('id_category' => $categoryId), 'id_it = :id', array(':id' => $idWordId));
        RuItLanguages::updateAll(['id_category' => $categoryId], ['id_it' => $idWordId]);
        $modelCategory = Category::find()->all();
        $isAllUsers = true;
        
        $data = $this->renderPartial('block/words', [
            'modelCategory' => $modelCategory,
            'isAllUsers' => $isAllUsers
                ]);
        echo json_encode(['status' => 'success', 'html' => $data]);
    }
    
    public function actionSetCustomCategory() {
        $itWords = Yii::$app->request->post('itWords', 0);
        $ruWords = Yii::$app->request->post('ruWords', 0);
        
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        $name = $modelUser->name;
        $customCategory = Category::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        if(empty($customCategory)) {
            $customCategory = new Category();
            $customCategory->user_id = $modelUser->id;
            $customCategory->category = 'C' . $modelUser->name;
            $customCategory->save();
        }
        
        $cId = $customCategory->id;
        $countWords = count($itWords);
        for($i = 0; $i < $countWords; $i++) {
            $RuIt = RuItLanguages::find()->where(['id_ru' => $ruWords[$i], 'id_it' => $itWords[$i], 'id_category' => $cId])->one();
            if(!empty($RuIt))
                continue;
            $RuIt = new RuItLanguages();
            $RuIt->id_ru = $ruWords[$i];
            $RuIt->id_it = $itWords[$i];
            $RuIt->id_category = $cId;
            $RuIt->save();
        }
    }
    
    public function actionCleanCustomCategory() {
        $categoryId = Category::find()->where(['user_id' => Yii::$app->user->identity->id])->one()->id;
        RuItLanguages::deleteAll(['id_category' => $categoryId]);
    }
    
    
    public function actionUpdateRuWord() {
        if(Yii::$app->user->identity->role != User::ROLE_SUPERADMIN)
            return;
        $ruWordId = Yii::$app->request->post('ruWordId', 0);
        $ruWord = Yii::$app->request->post('ruWord', 0);
        $modelRu = RuLanguage::find(['id' => $ruWordId]);
        $modelRu->word = $ruWord;
        $modelRu->save();
        //$data = $this->renderPartial('block/words', array('modelCategory' => $modelCategory, 'isAllUsers' => $isAllUsers), true);
        //echo json_encode(array('status' => 'success', 'html' => $data));
    }
    
    public function actionUpdateItWord() {
        if(Yii::app()->user->getUser()->role != User::ROLE_SUPERADMIN)
            return;
        $itWordId = Yii::$app->request->post('itWordId', 0);
        $itWord = Yii::$app->request->post('itWord', 0);
        $modelIt = ItLanguage::find(['id' => $itWordId]);
        $modelIt->word = $itWord;
        $modelIt->save();
        //$data = $this->renderPartial('block/words', array('modelCategory' => $modelCategory, 'isAllUsers' => $isAllUsers), true);
        //echo json_encode(array('status' => 'success', 'html' => $data));
    }
}