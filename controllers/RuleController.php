<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\Activity;

class RuleController extends Controller
{
    const NOUNS_TESTS = 1;
    
    public function actionIndex() {
        $data = $this->renderPartial('menu');
        return $this->render('index', ['data' => $data]);
    }
    
    public function actionAlphabet() {
        $data = $this->renderPartial('menu');
        return $this->render('alphabet', ['data' => $data]);
    }
    
    public function actionGrammar() {
        $data = $this->renderPartial('menu');
        return $this->render('grammar', ['data' => $data]);
    }
    
    public function actionNouns() {
        $data = $this->renderPartial('menu');
        return $this->render('nouns', ['data' => $data]);
    }
    
    public function actionNounstest() {
        return $this->render('tests/nounstest');
    }
    
    public function actionNounsGenerate() {
        $test = Yii::$app->request->post('test', -1);
        $testNum = Yii::$app->request->post('testNum', -1);
        if($test == -1 || $testNum == -1)
            die();
        
        if($test >= self::NOUNS_TESTS) {
            echo json_encode(['end' => true]);
            die();
        }
        
        //if(!$this->getViewFile('tests\\nouns\\test1' . $test)) { for local server
        if(!file_exists($this->getViewPath() . '/tests/nouns/test' . $testNum . $test .'.php')) {
            echo json_encode(['status' => false, 'end' => false]);
            die();
        }
        
        $dataTest = 'tests/nouns/test' . $testNum . $test;
        $data = $this->renderPartial($dataTest);
        echo json_encode(['status' => true, 'end' => false, 'data' => $data]);
    }
    
    public function actionAfterTest() {
        $experience = (int) Yii::$app->request->post('experience');
        
        $modelUser = User::findIdentity(Yii::$app->user->identity->id);
        
        $modelUser->experience += $experience;
        $modelUser->total_experience += $experience;

        $modelActivity = Activity::find()->where(['date' => date('Y-m-d'), 'user_id' => Yii::$app->user->identity->id])->one();
        if(empty($modelActivity)) {
            $modelActivity = new Activity;
            $modelActivity->date = date('Y-m-d');
            $modelActivity->user_id = Yii::$app->user->identity->id;
        }
        $modelActivity->gain_experience = $experience;

        $points = substr($experience, strlen($experience)-1);
        if($points == 0) {
            $pointsText = \Yii::t('main', 'очков');
        } elseif($points == 1) {
            $pointsText = \Yii::t('main', 'очко');
        } elseif($points >= 2 && $points <= 4) {
            $pointsText = \Yii::t('main', 'очкa');
        } elseif($points >= 5 && $points <= 9) {
            $pointsText = \Yii::t('main', 'очков');
        }
        $gainedExperienceText = '+' . $experience . ' ' . $pointsText;
        $gainedLevel = false;
        $gainedLevelText = '';
        if($modelUser->experience >= $modelUser->levels->experience) {
            $gainedLevel = true;
            $diffExperience = $modelUser->experience - $modelUser->levels->experience;
            $modelUser->experience = $diffExperience;
            $modelUser->level += 1;
            if(empty(json_decode($modelActivity->gain_achievements)->{"level"})) {
                $modelActivity->gain_achievements = json_encode(['level' => [['Reached level ', $modelUser->level]]]);
            } else {
                $levelAchieves = json_decode($modelActivity->gain_achievements);
                $levelAchieves->{"level"}[] = ['Reached level ', $modelUser->level];
                $modelActivity->gain_achievements = json_encode($levelAchieves);
            }
            $gainedLevelText = \Yii::t('main', 'Level up!');
        }
        $modelUser->save(false);
        $modelActivity->save(false);
        $headerProgress = $this->renderPartial('block/header_progress', [
            'userModel' => $modelUser
            ]);
        
        echo json_encode(['status' => true, 'end' => true, 'headerProgress' => $headerProgress]);
    }
}

