<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        'css/bootstrap/bootstrap.css',
        'css/bootstrap/bootstrap-theme.css',
        'css/select2/select2.min.css',
        'css/font-awesome/font-awesome.css'
    ];
    public $js = [
        'js/bootstrap/bootstrap.min.js',
        'js/meiomask/meiomask.min.js',
        'js/bootstrap/bootstrap-timepicker.js',
        'js/main.js',
        'js/select2/select2.full.min.js',
        'js/animateNumber/jquery.color.min.js',
        'js/animateNumber/jquery.animateNumber.min.js',
        'js/site/result.js',
        'js/rules/tests/test_result.js',
        'js/rules/sidebar.js',
        '//code.highcharts.com/highcharts.js',
        '//code.highcharts.com/modules/exporting.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
