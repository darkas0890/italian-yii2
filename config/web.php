<?php
session_start();
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => empty($_SESSION['lang']) ? 'ru' : $_SESSION['lang'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'h1M0uWBg3fxyt6F83WTlingPlpfxs6hm',
            'enableCsrfValidation' => false
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            //'class' => 'yii\caching\ApcCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'http://italianyii2.va/site/repeat/<language:\w{2}>/<category:\d+>' => 'site/repeat',
                'http://italianyii2.va/site/repeat/<language:\w{2}>' => 'site/repeat',
                'http://italianyii2.va/site/constructor/<category:\d+>' => 'site/constructor',
                'http://italianyii2.va' => 'site/index',
                'http://italianyii2.va/authorization' => 'site/authorization',
                'http://italianyii2.va/<controller:(\w|-)+>/<action:(\w|-)+>/<id:\d+>' => '<controller>/<action>',
                'http://italianyii2.va/<controller:(\w|-)+>/<action:(\w|-)+>/' => '<controller>/<action>',
                '<controller:(\w|-)+>/' => '<controller>/index',
                '<controller:\w+>/<action:(\w|-)+>/<id:\d+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:(\w|-)+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<action:(\w|-)+>' => '<controller>/<action>'
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'i18n' => [
            'translations' => [
                'main' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'main' => 'main.php',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
    'on beforeAction' => function ($event) {
        if (Yii::$app->user->isGuest && !Yii::$app->request->isAjax && !in_array("{$event->action->controller->id}/{$event->action->id}", ['site/captcha', 'site/index', 'site/home', 'site/sendmessage', 'site/authorization'])) {
            return $event->action->controller->redirect(['site/home']);
        }
    },
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
