<?php
$totalWords = 0;
?>
<form class="form-style-4" action="/superadmin/create" method="post">
    <div class="index-item-add-word">
        <div class="row">
            <div class="col-lg-10 col-xs-10">
                <div class="ml30">
                    <div class="index-item-category mb50 mt15">
                        <select class="cs-select cs-skin-overlay" name="Category">
                            <option value="" disabled <?php empty($selectedCategory) ? 'selected' : '' ?>><?php echo Yii::t('main', 'Select a category'); ?></option>
                            <optgroup label="<?php echo Yii::t('main', 'Category'); ?>">
                            <?php foreach($modelCategory as $key => $category): ?>
                                <option value="<?php echo $category->id ?>" <?php echo !empty($selectedCategory) && $selectedCategory == $category->id ? 'selected' : '' ?>><?php echo Yii::t('main', $category->category); ?></option>
                            <?php endforeach; ?>
                            </optgroup>
                            <optgroup label="<?php echo Yii::t('main', 'Words'); ?>">
                                <?php foreach($modelCategory as $key => $category): ?>
                                <?php $totalWords += count($category->RuLanguages); ?>
                                <option data-no-select><?php echo count($category->RuLanguages) . ' ' . Yii::t('main', 'words');?></option>
                            <?php endforeach; ?>
                            </optgroup>
                                <option data-no-select><?php echo $totalWords . ' ' . Yii::t('main', 'words');?></option>
                        </select>
                    </div>
                    <div class="index-item-translate">
                        <div class="index-item-translate-it mb45">
                            <label>
                                Versione italiana
                            </label>
                            <input type="text" name="ItInput" autocomplete="off">
                        </div>
                        <div class="index-item-translate-ru">
                            <label>
                                Русский вариант
                            </label>
                            <input type="text" name="RuInput" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-2">
                <div class="icon-new-word-div">
                    <i class="icons icon-new-word"></i>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="mt40" style="text-align: center;">
                    <button type="submit" class="button31"></button>
                </div>
            </div>
        </div>
    </div>
</form>