<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/site/tasks.js' . Controller::ANTICACHE);
$totalWords = 0;
?>
<div class="items">
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <?php $this->renderPartial('block/add_new_word', array('modelCategory' => $modelCategory, 'selectedCategory' => $selectedCategory)); ?>
        </div>
    </div>
</div>

<script>
    (function() {
        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
            new SelectFx(el, {
                stickyPlaceholder: false
            });
        } );
    })();
</script>