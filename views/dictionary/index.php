<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/dictionary/index.js');
?>
<div class="dictionary">
    <div class="dictionary-inner">
        <div class="dictionary-categories pull-left">
            <ul>
                <li class="dictionary-category"><?= \Yii::t('main', 'All words'); ?></li>
                <?php foreach($modelCategory as $key => $category): ?>
                    <li class="dictionary-category" data-category-id="<?= $category->id; ?>"><?= \Yii::t('main', $category->category); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="dictionary-words pull-left">
            <?= $this->render('block/words', ['modelCategory' => $modelCategory, 'isAllUsers' => $isAllUsers]) ?>
        </div>
        <div class="dictionary-footer pull-left">
            <h4><?= \Yii::t('main', 'Всего слов') . ' ' . $countWords; ?></h4>
        </div>
    </div>
</div>