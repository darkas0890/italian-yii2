<?php
use app\models\User;
?>
<div class="dictionary-words-head">
    <h3><?= \Yii::t('main', 'All words'); ?></h3>
</div>
<div class="dictionary-tools">
    <div class="dictionary-tool-filter">
        <input type="text" id="dictionary-filter" autocomplete="off" placeholder="<?= \Yii::t('main', 'Filter') ?>">
        <span class="icon search" data-content="<?= \Yii::t('main', 'Search'); ?>"></span>
    </div>
    <div class="selected-items-tools" style="display: none;">
        <i class="addToCustomCategory fa fa-plus" aria-hidden="true" data-toggle="popover" data-trigger="hover" rel="popover" data-content="Добавить отмеченные слова в свой набор" data-placement="bottom"></i>
    </div>
    <div class="custom-items-tools" style="display: none;">
        <i class="cleanCustomCategory fa fa-trash" aria-hidden="true" data-toggle="popover" data-trigger="hover" rel="popover" data-content="Добавить отмеченные слова в свой набор" data-placement="bottom"></i>
    </div>
</div>
<div class="dictionary-words-container">
    <?php if($isAllUsers): ?>
        <?php foreach($modelCategory as $category): ?>
            <?php foreach($category->itLanguages as $key => $word): ?>
            <div class="dictionary-item">
                <div class="dictionary-item-word">
                    <input type="checkbox" class="dictionary-item-select">
                    <div class="dictionary-item-word-it<?= Yii::$app->user->identity->role == User::ROLE_SUPERADMIN ? ' superadmin' : '' ?>" data-it-id="<?= $word->id; ?>">
                        <?= $word->word; ?>
                    </div>
                    <div class="dictionary-item-word-hyphen">
                        &nbsp; &mdash; &nbsp;
                    </div>
                    <div class="dictionary-item-word-ru<?= Yii::$app->user->identity->role == User::ROLE_SUPERADMIN ? ' superadmin' : '' ?>" data-ru-id="<?= $word->translates[0]->id; ?>">
                        <?= $word->translates[0]->word; ?>
                    </div>
                </div>
                <div class="dictionary-item-tools">
                    <div class="dictionary-item-category<?= Yii::$app->user->identity->role == User::ROLE_SUPERADMIN ? ' superadmin' : '' ?>" data-category-id="<?= $category->id; ?>">
                        <?= Yii::t('main', $category->category); ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <?php foreach($modelCategory as $category): ?>
            <?php foreach($category->itLanguages as $key => $word): ?>
            <div class="dictionary-item">
                <div class="dictionary-item-word">
                    <input type="checkbox" class="dictionary-item-select">
                    <div class="dictionary-item-word-it" data-it-id="<?= $word->id; ?>">
                        <?= $word->word; ?>
                    </div>
                    <div class="dictionary-item-word-hyphen">
                        &nbsp; &mdash; &nbsp;
                    </div>
                    <div class="dictionary-item-word-ru" data-ru-id="<?= $word->translates[0]->id; ?>">
                        <?= $word->translates[0]->word; ?>
                    </div>
                </div>
                <div class="dictionary-item-tools">
                </div>
            </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>