<?= $data; ?>
<div class="alphabet">
    <div class="alphabet-inner">
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Aa
                        </div>
                        <div class="alphabet-transcription">
                            [а]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/armadillo.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Armadillo
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Bb
                        </div>
                        <div class="alphabet-transcription">
                            [би]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/bisonte.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Bisonte
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Сс
                        </div>
                        <div class="alphabet-transcription">
                            [чи]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/cinghiale.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Cinghiale
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Dd
                        </div>
                        <div class="alphabet-transcription">
                            [ди]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/delfino.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Delfino
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Ee
                        </div>
                        <div class="alphabet-transcription">
                            [э]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/elefante.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Elefante
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Ff
                        </div>
                        <div class="alphabet-transcription">
                            [ф]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/farfalla.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Farfalla
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Gg
                        </div>
                        <div class="alphabet-transcription">
                            [джи]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/giraffa.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Giraffa
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Ii
                        </div>
                        <div class="alphabet-transcription">
                            [и]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/ippopotamo.jpeg" />
                        </div>
                        <div class="alphabet-translate">
                            Ippopotamo
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Ll
                        </div>
                        <div class="alphabet-transcription">
                            [л]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/leone.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Leone
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Mm
                        </div>
                        <div class="alphabet-transcription">
                            [м]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/maiale.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Maiale
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Nn
                        </div>
                        <div class="alphabet-transcription">
                            [н]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/nottola.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Nottola
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Oo
                        </div>
                        <div class="alphabet-transcription">
                            [о]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/orso.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Orso
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Pp
                        </div>
                        <div class="alphabet-transcription">
                            [п]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/pinguino.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Pinguino
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Qq
                        </div>
                        <div class="alphabet-transcription">
                            [к]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/quaglia.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Quaglia
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Rr
                        </div>
                        <div class="alphabet-transcription">
                            [р]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/rinoceronte.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Rinoceronte
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Ss
                        </div>
                        <div class="alphabet-transcription">
                            [s-с, s-з]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/scorpione.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Scorpione
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Tt
                        </div>
                        <div class="alphabet-transcription">
                            [т]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/tigre.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Tigre
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Uu
                        </div>
                        <div class="alphabet-transcription">
                            [у]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/usignolo.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Usignolo
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Vv
                        </div>
                        <div class="alphabet-transcription">
                            [в]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/visone.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Visone
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="alphabet-item">
                    <div class="alphabet-item-inner">
                        <div class="alphabet-letter">
                            Zz
                        </div>
                        <div class="alphabet-transcription">
                            [z-ц, z-дз]
                        </div>
                        <div class="alphabet-image">
                            <img src="/images//alphabet/zebra.jpg" />
                        </div>
                        <div class="alphabet-translate">
                            Zebra
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="other-letters">
                <b>Jj Kk Ww Xx Yy</b> - <?php echo Yii::t('main', 'they are not letters of Italian alphabet, and are mainly used for foreign words'); ?>.
                <br><br>
                <b>Hh</b> - <?php echo Yii::t('main', 'can not be read separately'); ?>.
            </div>
        </div>
    </div>
</div>