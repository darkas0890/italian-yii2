<?= $data; ?>
<div class="nouns">
    <div class="nouns-inner">
        <div class="row" style="height: 50px;">
            <div class="col-lg-12 col-xs-12">
                <div class="nouns-start-test">
                    <a class="start-test" href="/rule/nounstest">Пройти тест <font style="font-size: 24px">»</font></a>
                </div>
            </div>
        </div>
        <div class="row" style="height: 35px;">
            <div class="col-lg-12 col-xs-12">
                <div class="nouns-rules">
                    <h2>Род и число имен существительных</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="nouns-content2">
                    <div class="new-row mb15">
                        В итальянском языке имена существительные имеют два рода: <b>мужской (maschile)</b> 
                        и <b>женский (femminile)</b> и два числа: <b>единственное (singolare)</b> 
                        и <b>множественное (plurale)</b>.
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 65%">
                                    Правила
                                </th>
                                <th style="width: 35%">
                                    Примеры
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Грамматический род существительных в русском и итальянском 
                                    языках может не совпадать
                                </td>
                                <td>
                                    libro (м.р.) - книга (ж.р.)<br>
                                    classe (ж.р.) - класс (м.р.)<br>
                                    matita (ж.р.) - карандаш (м.р.)<br>
                                    gruppo (м.р.) - группа (ж.р.).
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Большинство существительных мужского рода оканчивается в 
                                    единственном числе на -о и во множественном числе на -i
                                </td>
                                <td>
                                    tavolo - стол<br>
                                    tavoli - столы.<br><br>
                                    <b>Исключения</b>: mano (ж.р.) - рука<br>
                                    radio (ж.р.) - радио.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные женского рода в единственном числе 
                                    оканчиваются на -а и во множественном числе на -е
                                </td>
                                <td>
                                    porta - дверь<br>
                                    porte - двери.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Большую группу составляют существительные мужского и 
                                    женского рода, оканчивающиеся в единственном числе 
                                    на -е и во множественном на -i
                                </td>
                                <td>
                                    padre - отец<br>
                                    padri - отцы<br>
                                    madre - мать<br>
                                    madri - матери.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные, оканчивающиеся на суффикс -ista 
                                    (обозначающие обычно профессию или партийную принадлежность), 
                                    в единственном числе имеют одинаковое окончание для мужского и 
                                    женского рода; во множественном числе они приобретают окончание 
                                    -i для мужского рода и -е для женского
                                </td>
                                <td>
                                    Socialista - социалист<br>
                                    socialisti - социалисты<br>
                                    Socialista - социалистка<br>
                                    socialiste - социалистки.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Все существительные, оканчивающиеся на суффикс -ione, - женского рода
                                </td>
                                <td>
                                    Situazione - ситуация<br>
                                    situazioni - ситуации.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные греческого происхождения, 
                                    оканчивающиеся на -i, - женского рода
                                </td>
                                <td>
                                    Crisi - кризис<br>
                                    Tesi - тезис.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные, оканчивающиеся на -tà, -tù, - женского рода
                                </td>
                                <td>
                                    Città - город<br>
                                    Virtù - добродетель.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные греческого происхождения на -ma, -ta, - 
                                    мужского рода; во множественном числе они оканчиваются на -i
                                </td>
                                <td>
                                    Problema - задача, проблема<br>
                                    roblemi - задачи, проблемы<br>
                                    Pianeta - планета<br>
                                    pianeti - планеты.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные, оканчивающиеся на -io с неударным -i, 
                                    во множественном числе второго -i не приобретают
                                </td>
                                <td>
                                    Vocabolario - словарь<br>
                                    vocabolari - словари<br>
                                    (НО: zío - дядя, zíi - дяди).
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="new-row mb15">
                        <b>НЕ меняют</b> окончания во множественном числе:
                    </div>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    1. Односложные существительные
                                </td>
                                <td width="35%">
                                    Dì - день<br>
                                    dì - дни.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2. Существительные, оканчивающиеся на ударную гласную
                                </td>
                                <td>
                                    Città - город<br>
                                    сittà - города.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    3. Cуществительные женского рода, оканчивающиеся на -i
                                </td>
                                <td>
                                    Crisi - кризис<br>
                                    crisi - кризисы.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    4. Слова иностранного происхождения
                                </td>
                                <td>
                                    Cinema - кинотеатр<br>
                                    cinema - кинотеатры<br>
                                    Gas - газ<br>
                                    gas - газы<br>
                                    Fílobus - троллейбус<br>
                                    fílobus - троллейбусы.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th style="width: 65%">
                                    Правила
                                </th>
                                <th style="width: 35%">
                                    Примеры
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Существительные, оканчивающиеся на -cia, -gia (i под ударением), 
                                    во множественном числе оканчиваются на -cie, -gie
                                </td>
                                <td>
                                    Farmacía (аптека) - farmacíe<br>
                                    Bugía (ложь) - bugíe.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Существительные, оканчивающиеся на -cia, -gia, 
                                    во множественном числе оканчиваются на -ce, -ge
                                </td>
                                <td>
                                    Cáccia (охота) - cácce<br>
                                    Ciliégia (вишня) - ciliégie<br>
                                    НО: Camícia (рубашка) - сamíciе.<br><br>
                                    <b>Запомните:</b>
                                    Uómo (ед. ч.) - человек, мужчина<br>
                                    Uómini (мн. ч.) - люди, мужчины.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
  
                <!--<div class="nouns-content">
                    <div class="new-row mb15">
                        В итальянском языке имена существительные имеют два рода: <b><i>мужской (maschile)</i></b> 
                        и <b><i>женский (femminile)</i></b> и два числа: <b><i>единственное (singolare)</i></b> 
                        и <b><i>множественное (plurale)</i></b>.
                    </div>

                    Грамматический род существительных в русском и итальянском языках может не совпадать:

                    <div class="nouns-example">
                        <span class="exclamation_point"></span>
                        <div class="example-text">
                            <span class="example_word">Пример:</span>
                            libro (м.р.) - книга (ж.р.), 
                            classe (ж.р.) - класс (м.р.), 
                            matita (ж.р.) - карандаш (м.р.), 
                            gruppo (м.р.) - группа (ж.р.).
                        </div>
                    </div>

                    <div class="new-row mb15">
                        Большинство существительных мужского рода оканчивается в единственном числе на <b><i>-о</i></b> 
                        и во множественном числе на <b><i>-i</i></b>:
                    </div>

                    <div class="nouns-example">
                        <span class="exclamation_point"></span>
                        <div class="example-text">
                            <span class="example_word">Пример:</span>
                            tavolo - стол, 
                            tavoli - столы.
                        </div>
                    </div>

                    <div class="nouns-except">
                        <span class="exclamation_point"></span>
                        <div class="example-text">
                            <span class="example_word">Исключение:</span>
                            mano (ж.р.) - рука,
                            radio (ж.р.) - радио.
                        </div>
                    </div>

                    <div class="new-row mb15">
                        А существительные женского рода в единственном числе оканчиваются на <b><i>-а</i></b> и во 
                        множественном числе на <b><i>-е</i></b>
                    </div>

                    <div class="nouns-example">
                        <span class="exclamation_point"></span>
                        <div class="example-text">
                            <span class="example_word">Пример:</span>
                            porta - дверь, 
                            porte - двери.
                        </div>
                    </div>

                    <div class="new-row mb15">
                        Большую группу составляют существительные мужского и женского рода, 
                        оканчивающиеся в единственном числе на <b><i>-е</b></i> и во множественном на <b><i>-i</b></i>
                    </div>

                    <div class="nouns-example">
                        <span class="exclamation_point"></span>
                        <div class="example-text">
                            <span class="example_word">Пример:</span>
                            padre - отец, 
                            padri - отцы, 
                            madre - мать, 
                            madri - матери.
                        </div>
                    </div>


                    Singolare	Plurale
                    tavolo	tavoli
                    porta	porte
                    padre	padri
                    madre	madri

                    Существительные, оканчивающиеся на суффикс -ista (обозначающие обычно профессию или партийную принадлежность), в единственном числе имеют одинаковое окончание для мужского и женского рода; во множественном числе они приобретают окончание -i для мужского рода и -е для женского:

                    Socialista - социалист, socialisti - социалисты
                    Socialista - социалистка, socialiste - социалистки

                    Все существительные, оканчивающиеся на суффикс -ione, - женского рода

                    Situazione - ситуация, situazioni - ситуации

                    Существительные греческого происхождения, оканчивающиеся на -i, - женского рода:

                    Crisi - кризис
                    Tesi - тезис

                    Существительные, оканчивающиеся на -tà, -tù, - женского рода:

                    Città - город
                    Virtù - добродетель

                    Существительные греческого происхождения на -ma, -ta, - мужского рода; во множественном числе они оканчиваются на -i:

                    Problema - задача, проблема, problemi - задачи, проблемы
                    Pianeta - планета, pianeti - планеты

                    Существительные, оканчивающиеся на -io с неударным -i, во множественном числе второго -i не приобретают:

                    Vocabolario - словарь, vocabolari - словари (НО: zío - дядя, zíi - дяди).

                    НЕ меняют окончания во множественном числе:

                    Односложные существительные:

                    Dì - день, dì - дни

                    Существительные, оканчивающиеся на ударную гласную:

                    Città - город, сittà - города

                    Cуществительные женского рода, оканчивающиеся на -i:

                    Crisi - кризис, crisi - кризисы

                    Слова иностранного происхождения:

                    Cinema - кинотеатр, cinema - кинотеатры
                    Gas - газ, gas - газы
                    Fílobus - троллейбус, fílobus - троллейбусы.

                    Существительные, оканчивающиеся на -cia, -gia (i под ударением), во множественном числе оканчиваются на -cie, -gie:

                    Farmacía (аптека) - farmacíe
                    Bugía (ложь) - bugíe

                    Существительные, оканчивающиеся на -cia, -gia, во множественном числе оканчиваются на -ce, -ge:

                    Cáccia (охота) - cácce
                    Ciliégia (вишня) - ciliégie
                    НО: Camícia (рубашка) - сamíciе

                    Запомните:

                    Uómo (ед. ч.) - человек, мужчина
                    Uómini (мн. ч.) - люди, мужчины.
                </div>
            </div>-->
        </div>
    </div>
</div>