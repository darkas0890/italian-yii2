<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/rules/index.js');
$type = empty($userModel) ? 0 : (int) $userModel->type;
$id = Yii::$app->user->identity->id;
$isAuthorized = (!empty($id)) ? true : false;
?>
<div class="sidebar">
    <span class="sidebar__close"></span>
    <ul class="sidebar-menu">
        <li class="<?= (Yii::$app->controller->id == 'rule' &&  Yii::$app->controller->action->id == 'alphabet') ? 'active' : '' ?>"><a href="/rule/alphabet"><span>Алфавит</span></a></li>
        <li class="<?= (Yii::$app->controller->id == 'rule' &&  Yii::$app->controller->action->id == 'nouns') ? 'active' : '' ?>"><a href="/rule/nouns"><span>Существительные</span></a></li>
    </ul>
</div>