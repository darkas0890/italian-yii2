<div class="test-question">
    Domenica penso di andare in … .
</div>
<div class="test-answers">
    <ol>
        <li><span>montagno</span></li>
        <li data-answer><span>montagna</span></li>
        <li><span>montagne</span></li>
    </ol>
</div>
<div class="test-answer">
    Domenica penso di andare in <span class="variant"></span> .
</div>