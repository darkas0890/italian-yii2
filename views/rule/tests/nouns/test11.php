<div class="test-question" style="">
    La … di spagnolo é facile.
</div>
<div class="test-answers">
    <ol>
        <li><span>leziono</span></li>
        <li><span>leziona</span></li>
        <li data-answer><span>lezione</span></li>
    </ol>
</div>
<div class="test-answer">
    La <span class="variant"></span> di spagnolo é facile.
</div>