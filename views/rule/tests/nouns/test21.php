<div class="test-question">
    Il … é giallo.
</div>
<div class="test-answers">
    <ol>
        <li data-answer><span>fiore</span></li>
        <li><span>fioro</span></li>
        <li><span>fiora</span></li>
    </ol>
</div>
<div class="test-answer">
    Il <span class="variant"></span> é giallo.
</div>