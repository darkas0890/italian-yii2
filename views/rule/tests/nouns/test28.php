<div class="test-question">
    Come fa caldo in queste ultime … !
</div>
<div class="test-answers">
    <ol>
        <li><span>settimana</span></li>
        <li data-answer><span>settimane</span></li>
        <li><span>settimani</span></li>
    </ol>
</div>
<div class="test-answer">
    Come fa caldo in queste ultime <span class="variant"></span> !
</div>