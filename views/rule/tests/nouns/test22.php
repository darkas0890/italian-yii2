<div class="test-question">
    La … é rossa.
</div>
<div class="test-answers">
    <ol>
        <li><span>cornicia</span></li>
        <li data-answer><span>cornicie</span></li>
        <li><span>cornicio</span></li>
    </ol>
</div>
<div class="test-answer">
    La <span class="variant"></span> é rossa.
</div>