<div class="test-question">
    Dalla finestra della mia camera spesso guardo la … che passa.
</div>
<div class="test-answers">
    <ol>
        <li><span>gento</span></li>
        <li data-answer><span>gente</span></li>
        <li><span>genta</span></li>
    </ol>
</div>
<div class="test-answer">
    Dalla finestra della mia camera spesso guardo la  <span class="variant"></span> che passa.
</div>