<div class="test-question">
    Il … é sereno, siamo ancora in estate, ma fra poco comincia l’autunno.
</div>
<div class="test-answers">
    <ol>
        <li data-answer><span>cielo</span></li>
        <li><span>ciela</span></li>
        <li><span>cieli</span></li>
    </ol>
</div>
<div class="test-answer">
    Il <span class="variant"></span> é sereno, siamo ancora in estate, ma fra poco comincia l’autunno.
</div>