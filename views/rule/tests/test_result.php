<div class="modal test-result" id="test-result-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="test-result-inner">
                <div class="test-result-progress">
                    <div class="test-result-progress-container">
                        <div class="test-result-progress-header">
                            <h2><?= \Yii::t('main', 'Поздравляю!') ?></h2>
                        </div>
                        <div class="test-stats-progress">
                            <svg style=" transform: rotate(-90deg); height: 210px; width: 210px;">
                            <circle cx="104" cy="104" r="94" stroke="whitesmoke" stroke-width="20" fill="none"></circle>
                            <circle class="test-svg-stats-progress" cx="104" cy="104" r="94" stroke="#54b574" 
                                    stroke-width="20" fill="none" stroke-dashoffset="0" 
                                    stroke-dasharray="0, 999"></circle>
                            </svg>
                            <span class="test-number-stats">
                                <span id="test-target-progress-experience" class="test-number-stats-progress" style="">0</span>
                                /<?= Yii::$app->user->identity->levels->experience ?>
                            </span>
                        </div>
                    </div>
                    <div class="test-result-progress-footer">
                        <div>
                            <button><a href="/rule/index">Вернуться к упражнениям</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>