<?php
use yii\helpers\Url;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/rules/tests/nouns.js');
?>
<div class="test">
    <div class="test-inner">
        <div class="test-back">
            <a href="<?= Url::toRoute('rule/nouns'); ?>">← Go back</a>
        </div>
        <div class="test-content">
        </div>
        <div class="test-footer">
            <div class="test-footer-content">
                <div class="test-status-bar">
                    <div class="test-task-status next">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i class=""></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                    <div class="test-task-status">
                        <i></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('test_result'); ?>