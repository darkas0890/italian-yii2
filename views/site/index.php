<?php
$this->title = 'Italiano';
?>
<div class="landing-header" id="header">
    <div class="header-container">
        <div class="header-logo"></div>
        <div class="header-text">
            Это лучший ресурс в изучении <br> 
            любимого Вами итальянского языка!
             
        </div>
        
        <div class="next">
            <div style="margin-top:10px">
                <a href="#about" class="font-white page-scroll">
                    <img src="<?php echo Yii::$app->request->baseUrl ?>/images/ico/arrow_down_circle.png">
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="stages-container" id="stages">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <i class="icon-culture"></i>
                <i class="icon-fun"></i>
                <i class="icon-brainstorm"></i>
                <i class="icon-missions"></i>
                <i class="icon-stories"></i>
            </div>
        </div>
    </div>
    <div class="about-container" id="about">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="section-heading">О ресурсе</h2>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                <div class="inner">
                    <div class="woman-bg"></div>
                    <p class="pt16y">"Я офигела, когда узнала, об этом ресурсе для изучения итальянского языка. 
                        Я опробовала его и это:
                        <strong>
                            <br><br>Быстро! 
                            <br>Эффективно! 
                            <br>Интересно! 
                            <br>Познавательно! 
                            <br>Увлекательно!"
                            <br>
                        </strong>
                    </p>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 text-left">
                <div class="about">На данный момент сайт находится на стадии разработки. 
                    Сейчас этот продукт предусматривает whitelist пользователей, которым доступен данный продукт.
                    Прежде всего необходимо рассказать, что будет данный продукт в себя включать:
                    <ul class="about-items">
                        <li><strong>Различные упражнения</strong>. Сейчас сайт содержит в себе только 3 упражнения, и в процессе разработки
                        они будут пополняться и будут более креативными и интересными. Правда на некоторые упражнения необходимо будет
                        <span class="premium">premium</span>-доступ.</li>
                        <li><strong>Словарь</strong>. Вы можете просматривать словарь, в нём слова разбиты по категориям. Категории пополняются 
                            и вы сможете создать свой собственный набор, в который вы внесете необходимые вам слова для изучения.</li>
                        <li><strong>Правила и тесты</strong>. Есть отдельный пункт правил. Самым первым и необходимым в изучении языка является знание алфавита, 
                        поэтому он стоит первым в списке. Список наполнен различными правилами итальянского языка, а так же каждое из них 
                        содержит в себе тест для закрепления знаний.</li>
                    </ul>
                    <div class="about-goodluck">Поэтому успехов Вам! И спасибо, что Вы с нами!</div>
                </div>
            </div>
        </div>
        <div class="next">
            <div style="margin-top:10px">
                <a href="#skills" class="font-white page-scroll">
                    <img src="<?php echo Yii::$app->request->baseUrl ?>/images/ico/arrow_down_circle.png">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="bg-skills">
    <div class="container">   
        <div class="inner-skills" id="skills">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h2 class="section-heading skills-heading">Наш ресурс содержит</h2>
                </div>
            </div>
            <div class="row" style="margin-bottom: 40px;">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="skill-block skill-block-left first-block">
                        <div class="skill-header">
                            <div class="skill-header-text">
                                Упражнения
                            </div>
                        </div>
                        <div class="skill-text">
                            Три упражнения по переводу слов с родного языка 
                            на итальянский и наоборот, а так же конструктор слова
                            <br>
                            <i class="skills-training"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="skill-block skill-block-right first-block">
                        <div class="skill-header">
                            <div class="skill-header-text">
                                Словарь
                            </div>
                        </div>
                        <div class="skill-text">
                            Словарь, в котором описаны, внесённые на данный момент, 
                            слова,отсортированные по категориям
                            <br>
                            <i class="skills-dictionary"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 40px;">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="skill-block skill-block-left second-block">
                        <div class="skill-header">
                            <div class="skill-header-text">
                                Правила
                            </div>
                        </div>
                        <div class="skill-text">
                            Правила итальянского языка, лексика и грамматика
                            <br>
                            <i class="skills-rules"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="skill-block skill-block-right second-block">
                        <div class="skill-header">
                            <div class="skill-header-text">
                                Тесты
                            </div>
                        </div>
                        <div class="skill-text">
                            Интересные, анимационные тесты по правилам 
                            для закрепления знаний
                            <br>
                            <i class="skills-tests"></i>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="next">
                <div style="margin-top:10px">
                    <a href="#contact" class="font-white page-scroll">
                        <img src="<?php echo Yii::$app->request->baseUrl ?>/images/ico/arrow_down_circle.png">
                    </a>
                </div>                    
            </div>
        </div>
    </div>
</div>
<div class="contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="contact-header">
                    <h1>Свяжитесь с нами!</h1>
                </div>
                <div class="contact-container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="contact-text">
                                Если у вас есть пожелания, комментарии или, может, 
                                <br>вы просто хотите оста-<br>вить отзыв, рассказать о ваших впечатлениях, 
                                то <br>мы с радостью прочитаем и постараемся вам <br>ответить!
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="contact-form">
                                <form method="POST" id="contactMe">
                                    <input class="contact-email" autocomplete="off" id="email" name="email" placeholder="E-mail" required>
                                    <textarea class="contact-comment" autocomplete="off" name="comment"
                                              id="comment" rows="15" cols="18" placeholder="Здесь оставляйте свой комментарий">
                                    </textarea>
                                    <button type="submit" class="contact-submit">Отправить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-another">
                    <div class="contact-another-header">
                        <h1>Другие способы связи</h1>
                    </div>
                    <div class="contact-another-container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <img src="https://lh3.googleusercontent.com/-wALGHEmPxek/AAAAAAAAAAI/AAAAAAAAABg/pRm7H7hgR9s/s0-c-k-no-ns/photo.jpg" />
                                <a href="http://www.upwork.com/o/profiles/users/_~0171546747c85d4325/" target="_blank" rel="nofollow noopener">Upwork</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <img src="https://lh6.ggpht.com/8-N_qLXgV-eNDQINqTR-Pzu5Y8DuH0Xjz53zoWq_IcBNpcxDL_gK4uS_MvXH00yN6nd4=w300" />
                                <a href="mailto:darkas0890@gmail.com">darkas0890@gmail.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
        </div>
        <div class="row">
            <div class="next">
                <div style="margin-top:10px">
                    <a href="#footer" class="font-white page-scroll">
                        <img src="<?php echo Yii::$app->request->baseUrl ?>/images/ico/arrow_down_circle.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
                <a href="#header" class="font-white page-scroll">
                    Вернуться &uarr;
                </a>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center">
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-center">
                <!-- © Easy Italian. --> <br> All rights reserved, 2016<br>
            </div>
        </div>
    </div>
</footer>

<script>
    $(document).ready(function() {
        window.headerLandingHeight = $('.landing-header').height();
        window.aboutHeight = $('.about-container').height();
        window.contactHeight = $('.contact-container').height();
        
        setTimeout(function(){$('.page-screen').fadeOut(600);}, 400);
        
        sideBarColor($(window));
        
        $('.contact-comment').text('');
        
        var $init = $('.skill-block-left, .skill-block-right, .skill-text, .skill-header-text');
        if ($init.length) {
            $init.bind('inview', function(event, isInView) {
                var $this = $(this);
                if (isInView || $(window).width() <= 1024) {
                    $this.addClass('in-viewport');
                //} else {
                //    $this.removeClass('in-viewport');
                }
            });
        }
        
        $(window).scroll(function(e) {
           sideBarColor($(this));
        });
    });
    
    $(function() {
        $('a.page-scroll,a.page-scroll2').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });
    
    function sideBarColor(object) {
        if(object.scrollTop() >= headerLandingHeight - 150 && object.scrollTop() < headerLandingHeight + aboutHeight - 150) {
            $('.home, .signin, .signup').css('background-color', 'rgba(0, 0, 0, 0.95)');
            $('.home a, .signin a, .signup a').css('color', 'white');
       } else if(object.scrollTop() >= headerLandingHeight + aboutHeight - 150 && object.scrollTop() < headerLandingHeight + aboutHeight + contactHeight - 150) {
            $('.home, .signin, .signup').css('background-color', 'rgba(194, 255, 197, 0.95)');
            $('.home a, .signin a, .signup a').css('color', 'black');
       } else if(object.scrollTop() >= headerLandingHeight + aboutHeight + contactHeight - 150) {
            $('.home, .signin, .signup').css('background-color', 'rgba(0, 0, 0, 0.95)');
            $('.home a, .signin a, .signup a').css('color', 'white');
       } else {
            $('.home, .signin, .signup').css('background-color', 'rgba(194, 255, 197, 0.95)');
            $('.home a, .signin a, .signup a').css('color', 'black');
       }
    }
    
    $("#contactMe").submit(function() {
        var form = $(this);
        $.ajax({
            url: '/site/sendmessage',
            type: 'post',
            data: form.serialize(),
            dataType: 'json',
            success: function(response) {
                if(response.status)
                    form[0].reset();
                console.log(response);
            }
        });
       return false;
    });
</script>
