<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/site/tasks.js');
$totalWords = 0;
$trainingLimit = Yii::$app->session->get('user.trainingLimit');
?>
<div class="items">
    <div class="no-mobile">
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="index-item-select-words mt10">
                    <div class="index-item-category mb38 mt40">
                        <select class="/*cs-select cs-skin-overlay*/ select2" name="Category" style="padding: 0px;">
                            <option value="" disabled selected><?= \Yii::t('main', 'Select a category for the study'); ?></option>
                            <optgroup label="<?= \Yii::t('main', 'Category'); ?>">
                            <?php foreach($modelCategory as $key => $category): ?>
                                <option value="<?= $category->id ?>" <?= !empty($selectedCategory) && $selectedCategory == $category->id ? 'selected' : '' ?>><?= \Yii::t('main', $category->category); ?></option>
                            <?php endforeach; ?>
                            <option value="100"><?= \Yii::t('main', 'All words'); ?></option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <form>
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="training-item-limit-info">
                        <?= \Yii::t('main', 'Количество слов в упражнениях'); ?>
                    </div>
                    <div class="training-item-limit">
                        <input type="radio" class="training-words-limit-radio" id="training-limit-1" 
                               name="radio" <?= $trainingLimit == 5 ? 'checked' : '' ?> onclick="limitState(5);">
                        <label class="training-item-limit-text" for="training-limit-1">5 <?= \Yii::t('main', 'words'); ?></label>
                    </div>
                    <div class="training-item-limit">
                        <input type="radio" class="training-words-limit-radio" id="training-limit-2" 
                               name="radio" <?= $trainingLimit == 10 ? 'checked' : '' ?> onclick="limitState(10);">
                        <label class="training-item-limit-text" for="training-limit-2">10 <?= \Yii::t('main', 'words'); ?></label>
                    </div>
                    <div class="training-item-limit">
                        <input type="radio" class="training-words-limit-radio" id="training-limit-3" 
                               name="radio" <?= $trainingLimit == 15 ? 'checked' : '' ?> onclick="limitState(15);">
                        <label class="training-item-limit-text" for="training-limit-3">15 <?= \Yii::t('main', 'words'); ?></label>
                    </div>
                    <div class="training-item-limit">
                        <input type="radio" class="training-words-limit-radio" id="training-limit-4" 
                               name="radio" <?= $trainingLimit == 20 ? 'checked' : '' ?> onclick="limitState(20);">
                        <label class="training-item-limit-text" for="training-limit-4">20 <?= \Yii::t('main', 'words'); ?></label>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="index-item index-item-repeat-ru mt10">
                    <div class="row" style="height: 140px;">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Русско-итальянский перевод'); ?></h3></div>
                            <button class="index-item-button btn btn-primary"><h3><?= \Yii::t('main', 'Start'); ?></h3></button>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-repeat"></i>
                        </div>
                        <div class="index-item-detailed-info">Тренировка "Повторение слов" помогает 
                            повторить и усвоить слова посредством перевода их с итальянского языка на родной.</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="index-item index-item-repeat-it mt10">
                    <div class="row" style="height: 140px;">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Итальянско-русский перевод'); ?></h3></div>
                            <button class="index-item-button btn btn-primary"><h3><?= \Yii::t('main', 'Start'); ?></h3></button>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-repeat"></i>
                        </div>
                        <div class="index-item-detailed-info">Тренировка "Повторение слов" помогает 
                            повторить и усвоить слова посредством перевода их с итальянского языка на родной.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-xs-6">
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="index-item index-item-constructor mt10">
                    <div class="row">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Constructor');?></h3></div>
                            <button class="index-item-button btn btn-primary"><h3><?= \Yii::t('main', 'Start'); ?></h3></button>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-study"></i>
                        </div>
                        <div class="index-item-detailed-info">Вы можете добавлять новые слова в словарь 
                            для дальнейшего их изучения. Особенно важно сортировать слова по 
                            разным категориям - это упрощает их изучение, а так же 
                            можно воспроизводить уроки для повторения слов, 
                            используя конкретные категории.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="index-item-select-words mt10">
                    <div class="index-item-category mb38 mt40">
                        <select class="cs-select cs-skin-overlay" name="Category">
                            <option value="" disabled selected><?= \Yii::t('main', 'Select a category for the study'); ?></option>
                            <optgroup label="<?= \Yii::t('main', 'Category'); ?>">
                            <?php foreach($modelCategory as $key => $category): ?>
                                <option value="<?= $category->id ?>" <?= !empty($selectedCategory) && $selectedCategory == $category->id ? 'selected' : '' ?>><?= \Yii::t('main', $category->category); ?></option>
                            <?php endforeach; ?>
                            <option value="100"><?= \Yii::t('main', 'All words'); ?></option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <form>
            <div class="training-item-limit-info">
                <?= \Yii::t('main', 'Количество слов в упражнениях'); ?>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="training-item-limit">
                            <input type="radio" class="training-words-limit-radio" id="training-limit-1" 
                                   name="radio" <?= $trainingLimit == 5 ? 'checked' : '' ?> onclick="limitState(5);">
                            <label class="training-item-limit-text" for="training-limit-1">5 </label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="training-item-limit">
                            <input type="radio" class="training-words-limit-radio" id="training-limit-2" 
                                   name="radio" <?= $trainingLimit == 10 ? 'checked' : '' ?> onclick="limitState(10);">
                            <label class="training-item-limit-text" for="training-limit-2">10 </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="training-item-limit">
                            <input type="radio" class="training-words-limit-radio" id="training-limit-3" 
                                   name="radio" <?= $trainingLimit == 15 ? 'checked' : '' ?> onclick="limitState(15);">
                            <label class="training-item-limit-text" for="training-limit-3">15 </label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="training-item-limit">
                            <input type="radio" class="training-words-limit-radio" id="training-limit-4" 
                                   name="radio" <?= $trainingLimit == 20 ? 'checked' : '' ?> onclick="limitState(20);">
                            <label class="training-item-limit-text" for="training-limit-4">20 </label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="index-item index-item-repeat-ru mt10">
                    <div class="row" style="height: 140px;">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Русско-итальянский перевод'); ?></h3></div>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-repeat"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="index-item index-item-repeat-it mt10">
                    <div class="row" style="height: 140px;">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Итальянско-русский перевод'); ?></h3></div>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-repeat"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="index-item index-item-constructor mt10">
                    <div class="row">
                        <div class="index-item-description col-lg-8 col-xs-8" style="height: 100%;">
                            <div class="index-item-title" style="padding-top: 30px; padding-left: 40px"><h3><?= \Yii::t('main', 'Constructor');?></h3></div>
                            <i class="index-item-info fa fa-info-circle fa-2x"></i>
                        </div>
                        <div class="col-lg-4 col-xs-4" style="height: 100%;">
                            <i class="icons icon-study"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>