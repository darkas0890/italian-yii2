<?php
use yii\helpers\Url;

?>
<div class="constructor">
    <div class="constructor-head">
        <div class="constructor-head-content">
            <a href="<?= Url::toRoute('site/tasks') ?>" class="iconm-back-link" data-back-train-btn> </a>
            <div class="constructor-head-title">
                <?= \Yii::t('main', 'Constructor'); ?>
            </div>
        </div>
        <div class="constructor-head-progress">
            <div class="constructor-progress-bar">
                <div class="constructor-current-progress-bar">

                </div>
            </div>
            <span class="constructor-progress-number">
                <span class="constructor-current-progress-number">0</span>
                /
                <span class="constructor-total-progress-number"><?= $trainingLimit; ?></span>
            </span>
        </div>
    </div>
    <div class="constructor-inner">
        <div class="constructor-inner-bg">
            <div class="constructor-content">
                <?= $this->render('block/constructor_word', [
                    'word' => $word,
                    'answersCount' => 0
                    ]); ?>
            </div>
        </div>
    </div>
</div>