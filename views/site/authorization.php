<?php
use app\components\AuthFormWidget;
?>
<style>
    body {
        min-width: 0 !important;
    }
    .auth-container {
        background: url('../images/italy_auth_bg.jpg');
        opacity: 0.9;
        background-attachment: scroll;
        background-size: 100% 100%;
        background-position: right center;
        background-repeat: no-repeat;
        width: 100%;
    }
    
    .auth-bg {
        height: 100vh;
        display: table;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    </style>
<div class="auth-container">
    <div class="auth-bg">
    <?php echo AuthFormWidget::widget(['startForm' => 'login']); ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.page-screen').hide();
        setTimeout(function(){ $('.page-screen').fadeOut(600); }, 400);
    });
</script>