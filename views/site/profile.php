<?php

use yii\helpers\Url;

$total = [];
$dates = [];
$totalPerDay = [];
$allAchieves = [];
$count = 0;
$countTotalExp = 0;

$dayNum = date('d');
$dayNum7Ago = $dayNum - 7;
//Activity date DESC
foreach($modelActivity as $activity) {
    $time = strtotime($activity->date);
    $currentDayNum = date('d', $time);
    if($currentDayNum <= $dayNum && $currentDayNum >= $dayNum7Ago) {
        array_unshift($totalPerDay, array(date('d/m/y', $time), (int)$activity->gain_experience));

        $achievements = json_decode($activity->gain_achievements);
        if(!empty($achievements)) {
            foreach($achievements as $std => $arr){
                if($std == 'level') {
                    foreach($arr as $key => $achievement) {
                        $allAchieves[date('d M Y', $time)][] = Yii::t('main', trim($achievement[0])) . " $achievement[1]";
                    }
                }
            }
        }
    }
}
$count = count($user->activities) - 1;
$interval = floor($count/7);
$chartDates = array(0, $interval, 2*$interval, 3*$interval, 4*$interval, 5*$interval, $count);
//Activity date ASC
for($i = 0; $i <= $count; $i++) {
    $countTotalExp += (int)$user->activities[$i]->gain_experience;
    if(in_array($i, $chartDates)) {
        $time = strtotime($user->activities[$i]->date);
        $total[] = $countTotalExp;
        $dates[] = date('d/m/y', $time);
    }
}

$total = json_encode($total);
$dates = json_encode($dates);
$totalPerDay = json_encode($totalPerDay);
?>
<div class="profile">
    <div class="profile-inner">
        <div class="profile-content">
            <div class="user-short_info">
                <div class="user-short_info_name"><?= $user->name; ?></div>
                <div class="user-short_info_email"><?= $user->email; ?></div>
                <div class="user-short_info_avatar">
                    <img src='<?= (!empty($user) && !empty($user->file_avatar)) ? "/uploads/{$user->file_avatar}" : "/images/default-avatar.png"; ?>' class="avatar-image"></div>
            </div>
            <div class="user-full_info">
                <div class="profile-language_settings">
                    <div class="profile-language">
                        <a class="icon-flag language-en" href="<?= Url::to(['/site/lang/', 'lang' => 'en']); ?>"></a>
                    </div>
                    <div class="profile-language">
                        <a class="icon-flag language-ru" href="<?= Url::to(['/site/lang/', 'lang' => 'ru']); ?>"></a>
                    </div>
                    <div class="profile-language">
                        <a class="icon-flag language-it" href="<?= Url::to(['/site/lang/', 'lang' => 'it']); ?>"></a>
                    </div>
                </div>
                <div class="user-info">
                    <h3><?= \Yii::t('main', 'Profile'); ?></h3>
                    <?= \Yii::t('main', 'SUPERADMIN') ?>
                    <div class="user-info-progress">
                        <?= \Yii::t('main', 'Total points') ?>
                    </div>
                    <?= $user->total_experience ?>
                    <div class="user-info-country">
                            <?= \Yii::t('main', 'profile.country') ?>
                    </div>
                    <?= $user->country ?>
                    <?php if(0): ?>
                    <select>
                        <?php foreach($countries as $country): ?>
                        <option value="<?= $country['id'] ?>"><?= $country['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php endif; ?>
                    <div class="user-info-town">
                            <?= \Yii::t('main', 'Town') ?>
                    </div>
                    <?= $user->town ?>
                    <div class="user-info-sex">
                            <?= \Yii::t('main', 'Sex') ?>
                    </div>
                    <?= \Yii::t('main', $user->sex) ?>
                    <br>
                    <br>
                </div>
                    <h3><?= \Yii::t('main', 'Charts'); ?></h3>
                        <?php if(!empty($user->activities)): ?>
                    <div class="profile-charts">
                        <div id="container" style="min-width: 310px; height: 400px; margin: 18px auto"></div>
                        <div id="container1" style="min-width: 310px; height: 400px; margin: 18px auto"></div>
                    </div>
                        <?php endif; ?>
                    
                    <?php if(!empty($allAchieves)): ?>
                <div class="profile-achievementes">
                    <h3><?= \Yii::t('main', 'Achievements'); ?></h3>
                    <?php foreach($allAchieves as $dateAchieve => $achieves): ?>
                    <div class="achievement">
                        <div class="achievement-date">
                        <?= $dateAchieve . ' (' . date('l', strtotime($dateAchieve)) . ')'; ?>
                        </div>
                        <br>
                        <?php foreach($achieves as $achieve): ?>
                        <div class="achievement-info">
                        <?= $achieve . '<br>'; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endforeach; ?>
                </div>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script>
    var dates = eval(<?= $dates ?>);
    var total = eval(<?= $total ?>);
    var totalPerDay = eval(<?= $totalPerDay ?>);
    
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'area',
            backgroundColor: ''
        },
        title: {
            text: 'Показатель изученных слов за всё время'
        },
        xAxis: {
            categories: dates,
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: 'Изученные слова'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' слов'
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#1b9c12',
                color: '#1b9c12',
                lineWidth: 1,
                shadow: true,
                marker: {
                    lineWidth: 1,
                    lineColor: '#1b9c12',
                    symbol: 'diamond',
                    radius: 4
                }
            }
        },
        series: [{
            name: 'Изученных слов',
            data: total
        }]
    });
});

$(function () {
    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Показатель изученных слов за последнюю неделю'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Количество изученных слов'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>{point.y:f} слов</b>'
        },
        series: [{
            name: 'Слова',
            data: totalPerDay,
            dataLabels: {
                enabled: true,
                rotation: -1,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>