<?php
use yii\helpers\Url;

$this->registerJs('var words = ' . json_encode($words) . ';', yii\web\View::POS_BEGIN);

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/site/repeat.js');
$answerId = rand(0, 4);
$trainingLimit = Yii::$app->session->get('user.trainingLimit');
?>
<div class="training">
    <div class="training-head">
        <div class="training-head-content">
            <a href="<?= Url::toRoute('site/tasks') ?>" class="iconm-back-link" data-back-train-btn> </a>
            <div class="training-head-title">
                <?= \Yii::t('main', 'Repeat'); ?>
            </div>
        </div>
        <div class="training-head-progress">
            <div class="training-progress-bar">
                <div class="training-current-progress-bar">

                </div>
            </div>
            <span class="training-progress-number">
                <span class="training-current-progress-number">0</span>
                /
                <span class="training-total-progress-number"><?= $trainingLimit; ?></span>
            </span>
        </div>
    </div>
    <div class="training-inner">
        <?= $this->render('block/training', [
            'answerId' => $answerId, 
            'mainWord' => $mainWord,
            'translateWords' => $translateWords,
            'answersCount' => 0
            ]); ?>
    </div>
</div>