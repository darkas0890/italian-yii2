<?php
$answerId = rand(0, 4);
$wordsInCase = [];
?>
<div class="training-inner-bg">
    <div class="training-content">
        <div class="no-mobile">
            <div class="col-lg-5 col-xs-12 col-md-12 col-sm-12">
                <h3 class="training-word" data-answers-count="<?= $answersCount; ?>" data-answer-item="<?= $words[0]['main_id'];?>"><?= \Yii::t('main', $words[0]['main_word']) ?></h3>
            </div>
            <div class="col-lg-7 col-xs-7 col-md-7 col-sm-7">
                <div class="col-lg-9 col-xs-9 col-md-9 col-sm-9">
                    <div class="training-translate-list">
                        <ul>
                            <?php for($i = 0; $i < 5; $i++): ?>
                                <?php if($i == $answerId): ?>
                                    <li data-answer-item="<?= $words[0]['translate_id']; ?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $words[0]['translate_word']; ?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php elseif($i == 0): ?>
                                    <li data-answer-item="<?= $words[$answerId]['translate_id']; ?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $words[$answerId]['translate_word']; ?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php else: ?>
                                    <li data-answer-item="<?= $words[$i]['translate_id']; ?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $words[$i]['translate_word']; ?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <li class="last">
                                <a href="javascript: void 0" class="btn next btn-blue" data-skip-item id="skip-item">
                                    <b>Не знаю :(</b>
                                </a> 
                                <!--<span class="item-number">1</span>-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                    <div class="training-timer">
                        <div class="training-round"></div>
                        <div class="training-timer-text">
                            5
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>