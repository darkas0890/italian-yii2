<?php
$wordsInCase = [];
?>
<div class="training-inner-bg">
    <div class="training-content">
        <div class="no-mobile">
            <div class="col-lg-5 col-xs-12 col-md-12 col-sm-12">
                <h3 class="training-word" data-answers-count="<?= $answersCount; ?>" data-answer-item="<?= $mainWord->id;?>"><?= \Yii::t('main', $mainWord->word) ?></h3>
            </div>
            <div class="col-lg-7 col-xs-7 col-md-7 col-sm-7">
                <div class="col-lg-9 col-xs-9 col-md-9 col-sm-9">
                    <div class="training-translate-list">
                        <ul>
                            <?php for($i = 0; $i < 5; $i++): ?>
                                <?php 
                                do {
                                $translateWordId = rand(0, count($translateWords) - 1);
                                } while(in_array($translateWords[$translateWordId]->id, $wordsInCase) || $translateWords[$translateWordId]->id == $mainWord->translates[0]->id);
                                array_push($wordsInCase, $translateWords[$translateWordId]->id);
                                ?>
                                <?php if($i == $answerId): ?>
                                    <li data-answer-item="<?= $mainWord->translates[0]->id;?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $mainWord->translates[0]->word?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php else: ?>
                                    <li data-answer-item="<?= $translateWords[$translateWordId]->id;?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $translateWords[$translateWordId]->word?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <li class="last">
                                <a href="javascript: void 0" class="btn next btn-blue" data-skip-item id="skip-item">
                                    <b>Не знаю, я лох :(</b>
                                </a> 
                                <!--<span class="item-number">1</span>-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                    <div class="training-timer">
                        <div class="training-round"></div>
                        <div class="training-timer-text">
                            5
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="mobile">
            <div class="row">
                <div class="col-md-9 col-xs-9">
                    <h3 class="training-word" data-answers-count="<?= $answersCount; ?>" data-answer-item="<?= $mainWord->id;?>"><?= \Yii::t('main', $mainWord->word) ?></h3>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="training-timer">
                        <div class="training-timer-text">
                            7
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="training-translate-list">
                        <ul>
                            <?php for($i = 0; $i < 5; $i++): ?>
                                <?php 
                                do {
                                $translateWordId = rand(0, count($translateWords) - 1);
                                } while(in_array($translateWords[$translateWordId]->id, $wordsInCase) || $translateWords[$translateWordId]->id == $mainWord->translates[0]->id);
                                array_push($wordsInCase, $translateWords[$translateWordId]->id);
                                ?>
                                <?php if($i == $answerId): ?>
                                    <li data-answer-item="<?= $mainWord->translates[0]->id;?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $mainWord->translates[0]->word?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php else: ?>
                                    <li data-answer-item="<?= $translateWords[$translateWordId]->id;?>">
                                        <a href="javascript: void 0" class="btn">
                                            <b><?= $translateWords[$translateWordId]->word?></b>
                                        </a> 
                                        <!--<span class="item-number">1</span>-->
                                    </li>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <li class="last">
                                <a href="javascript: void 0" class="btn next btn-blue" data-skip-item id="skip-item">
                                    <b>Не знаю, я лох :(</b>
                                </a> 
                                <!--<span class="item-number">1</span>-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>