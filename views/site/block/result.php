<?php
use yii\helpers\Url;
use app\models\ItLanguage;
?>
<div class="result">
    <div class="result-inner">
        <div class="result-words">
            <div class="result-words-container">
                <div class="result-words-mistakes">
                    <div class="result-words-mistakes-title">
                        <h4><?= \Yii::t('main', 'I don\'t know'); ?></h4>
                    </div>
                    <div class="result-words-mistakes-container">
                        <?php foreach($words as $wordId): ?>
                            <?php if(!in_array($wordId, $rightAnswers)): ?>
                            <div class="result-mistake">
                                <?= ItLanguage::findOne(['id' => $wordId])->word ?> - <?= ItLanguage::findOne(['id' => $wordId])->translates[0]->word ?>
                            </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="result-words-correct">
                    <div class="result-words-correct-title">
                        <h4><?php echo Yii::t('main', 'I know'); ?></h4>
                    </div>
                    <div class="result-words-correct-container">
                        <?php foreach($rightAnswers as $rightAnswerId): ?>
                        <div class="result-correct">
                            <?= ItLanguage::findOne(['id' => $rightAnswerId])->word ?> - <?= ItLanguage::findOne(['id' => $rightAnswerId])->translates[0]->word ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-progress">
            <div class="result-progress-container">
                <div class="result-progress-header">
                    <h2><?= \Yii::t('main', 'Поздравляю!') ?></h2>
                </div>
                <div class="stats-progress">
                    <svg style=" transform: rotate(-90deg); height: 210px; width: 210px;">
                    <circle cx="104" cy="104" r="94" stroke="whitesmoke" stroke-width="20" fill="none"></circle>
                    <circle class="svg-stats-progress" cx="104" cy="104" r="94" stroke="#54b574" 
                            stroke-width="20" fill="none" stroke-dashoffset="0" 
                            stroke-dasharray="0, 999"></circle>
                    </svg>
                    <span class="number-stats">
                        <span id="target-progress-experience" class="number-stats-progress" style="">0</span>
                        /<?= \Yii::$app->user->identity->levels->experience ?>
                    </span>
                </div>
            </div>
            <div class="result-progress-footer">
                <div>
                    <button><a href="<?= Url::toRoute('site/tasks') ?>">Вернуться к тренировкам</a></button>
                </div>
                <div>
                    <button class="continue"><a href="javascript: void(0)" onClick="location.reload();">Продолжить тренировку</a></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="flash-message">
<?php if((int)substr($gainedExperienceText, 1,1)): ?>
    <div class="flash-gained-experience">
        <div class="flash-gain-exp-img">
        </div>
        <div class="flash-experience-show">
            <?= $gainedExperienceText; ?>
        </div>
    </div>
    <?php if($gainedLevel): ?>
    <div class="flash-gained-level">
        <div class="flash-gain-level-img">
        </div>
        <div class="flash-level-show">
            <?=$gainedLevelText; ?>
        </div>
    </div>
    <?php endif; ?>
<?php endif; ?>
</div>