<?php
$letters = str_split($word->word);
$jsonWord = json_encode($letters);
shuffle($letters);
?>
<div class="constructor-content-head">
    <h3 data-answers-count="<?= $answersCount; ?>" data-answer-id="<?= $word->id ?>"><?= $word->translates[0]->word; ?></h3>
</div>
<div class="constructor-letters-answer">
    <?php foreach($letters as $letter): ?>
        <div class="constructor-letter">
            <input data-letter-input="" maxlength="0" value="" placeholder="" class="puzzleInput ">
        </div>
    <?php endforeach; ?>
</div>
<div class="constructor-letters-container">
    <?php foreach($letters as $letter): ?>
    <div class="constructor-letter"><?= strtolower($letter); ?></div>
    <?php endforeach; ?>
</div>

<script>
    var word = eval(<?= $jsonWord ?>);
    var wordLength = word.length;
    $('.constructor-letters-container .constructor-letter').bind('click', checkWord);
    $(document).bind('keyup', checkWordKeyUp);
    $(document).bind('keydown', checkWordKeyDown);
    $('.constructor-letter:eq(0)').addClass('next');
    
    function checkWord(letter = null) {
        var container = $(this),
            next = $('.next').index(),
            elem = $('.constructor-letter:eq(' + next + ')');
    
        if(letter.type != "click") {
            $('.constructor-letters-container .constructor-letter').each(function(indexOf, el) {
                var keyLetter = $('.constructor-letters-container .constructor-letter:eq(' + indexOf + ')');
                if(keyLetter.html() == letter && !keyLetter.hasClass('vhidden'))
                    container = keyLetter;
            });
        } 
        
        if(letter == null || letter.type == "click") {
            letter = $.trim(container.html());
            if(letter == '')
                letter = ' ';
        }
        
        if(letter == word[next].toLowerCase()) {
            $('.constructor').find('.next').next('.constructor-letter').addClass('next');
            container.addClass('vhidden');
            elem.addClass('constructor-correct-letter');
            elem.html(letter);
            elem.removeClass('next');
            if(next == wordLength - 1) {
                var LC = parseLanguageAndCategory();
                console.log();
                $.ajax({
                    url: '/site/generate-new-word',
                    type: 'POST',
                    data: {
                        answersCount: $('.constructor-content-head h3').data('answers-count') + 1,
                        answerId: $('.constructor-content-head h3').data('answer-id'),
                        rightAnswer: typeof $('.constructor').find('.constructor-content-head h3').data('wrong-answer') == 'undefined' ? 1 : 0,
                        category: LC[1]
                    },
                    dataType: 'json',
                    success: function(data) {
                        var totalProgress = $('span.constructor-total-progress-number').html();
                        $('span.constructor-current-progress-number').html(data.progress);
                        $('.constructor-current-progress-bar').css('width', data.progress*100/totalProgress + "%");
                        $(".constructor-current-progress-bar").css("transition", "0.4s cubic-bezier(0.75, 0.11, 1, 1)");
                        if(data.end) {
                            setTimeout(function() {
                                //setTimeout(function() { window.location.href = '/site/tasks'; }, 500);
                                $('.main').find('.constructor').slideUp(350);
                                setTimeout(function() { 
                                    $('.main').html(data.result); 
                                    $('.cursorTooltip').html(data.nextLevelIn);
                                    tooltipHover();
                                    setTimeout(makeResult, 300);
                                    $('.main').find('.result').slideDown(350);
                                    $(document).find('.header-progress').html(data.headerProgress);
                                    setTimeout(setLevelProgress, 200);
                                    toggleExperience(false);
                                    setTimeout(function() { toggleLevel(false) }, 900);}, 
                                350);
                                return;
                            },
                            500);
                        } else {
                            $('.constructor-content').html(data.html);
                        }
                    }
                });
                $('.constructor-letters-container .constructor-letter').unbind('click', checkWord);
                $(document).unbind('keyup', checkWordKeyUp);
            }
        } else {
            container.addClass('constructor-wrong-letter');
            $('.constructor').find('.constructor-content-head h3').attr('data-wrong-answer', '');
            setTimeout(function() { container.removeClass('constructor-wrong-letter'); }, 200);
        }
    }
    
    function checkWordKeyUp() {
        checkWord(event.key);
    }
    
    function checkWordKeyDown() {
        if(event.keyCode == 8 || event.keyCode == 32)
            event.preventDefault();
    }
</script>