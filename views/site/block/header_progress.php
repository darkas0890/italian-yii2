<div class="profile-level">
    <?= \Yii::t('main', 'Level'); ?> <?= !empty($userModel) ? $userModel->level : '0'?>
</div>
<div class="profile-experience">
    <div class="profile-experience-progress" 
         data-experience-progress="<?= !empty($userModel) ? $userModel->experience : 0; ?>" 
         data-experience-total="<?= !empty($userModel) ? $userModel->levels->experience : 0; ?>">
    </div>
    <div class="profile-experience-show">
        <?php
        $pointsText = \Yii::t('main', 'очков');
        if(!empty($userModel)) {
            $points = substr($userModel->experience, strlen($userModel->experience)-1);
            if($points == 0) {
                $pointsText = \Yii::t('main', 'очков');
            } elseif($points == 1) {
                $pointsText = \Yii::t('main', 'очко');
            } elseif($points == 2 || $points == 3 || $points == 4) {
                $pointsText = \Yii::t('main', 'очкa');
            } elseif($points == 5 || $points == 6 || $points == 7 || $points == 8 || $points == 9) {
                $pointsText = \Yii::t('main', 'очков');
            }
        }
        ?>
        <?= !empty($userModel) ? $userModel->experience : '0'?> <?= $pointsText; ?>
    </div>
</div>