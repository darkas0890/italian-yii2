<?php
use yii\helpers\Html;

$this->title = $name;
?>
<div style="padding: 10px 0 0 50px">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="error">
    <?php echo Html::encode($message); ?>
    </div>
</div>