<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\LandingAsset;

LandingAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <!-- blueprint CSS framework -->
        <link href="<?= Yii::$app->request->baseUrl; ?>/images/favicon-flag_italy.png" rel="icon" type="image/x-icon" />

        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="page-screen">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/page_loader.gif" />
        </div>
        <div class="home">
            <a href="<?= Url::to('site/home') ?>">Главная</a>
        </div>
        
        <div class="signin">
            <a href="<?= Url::to('authorization') ?>">Войти</a>
        </div>
        <?= $content ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>