<!DOCTYPE html>
<?php
use yii\widgets\ActiveForm;
use app\models\LoginForm;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;

$model = new LoginForm;
$id = !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->id : false;
$userModel = User::findIdentity($id);

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<html lang="en">
    <head>

        <!-- blueprint CSS framework -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
        <link href="<?php echo Yii::$app->request->baseUrl; ?>/images/favicon-flag_italy.png" rel="icon" type="image/x-icon" />

    </head>
    <body>
         <?php $this->beginBody() ?>
        <div class="wrap">
            <div class="content">
                <div class="header">
                    <div class="header-brand">
                        <div class="brand">
                            <i class="icon icon-brand"></i><div class="header-brand-text">Italian 100%</div>
                        </div>
                    </div>
                    <ul>
                        <?php if(Yii::$app->user->isGuest): ?>
                        <li><a href="<?= Url::toRoute('authorization') ?>"><?= 'Войти'; ?></a></li>
                        <li><a href="#"><?= 'Зарегистрироваться'; ?></a></li>
                        <?php else: ?>
                        <li><a href="<?= Url::toRoute('/site/logout') ?>"><?= 'Выйти'; ?></a></li>
                        <li><a href="<?= Url::toRoute('/site/home') ?>"><?= 'Главная'; ?></a></li>
                        <li><a href="<?= Url::toRoute('/dictionary/index') ?>"><?= 'Словарь'; ?></a></li>
                        <li><a href="<?= Url::toRoute('/site/tasks') ?>"><?= 'Тренировка'; ?></a></li>
                        <li><a href="<?= Url::toRoute('/rule/index') ?>"><?= 'Правила'; ?></a></li>
                            <?php if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN): ?>
                            <li><a href="<?= Url::toRoute('/superadmin/index') ?>"><?= 'Суперадмин'; ?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                    <div class="header-profile">
                        <div class="header-progress">
                            <div class="profile-level">
                                <?= \Yii::t('main', 'Level'); ?> <?= !empty($userModel) ? $userModel->level : '0'?>
                            </div>
                            <div class="profile-experience">
                                <div class="profile-experience-progress" 
                                     data-experience-progress="<?= !empty($userModel) ? $userModel->experience : 0; ?>" 
                                     data-experience-total="<?= !empty($userModel) ? $userModel->levels->experience : 0; ?>">
                                </div>
                                <div class="profile-experience-show">
                                    <?php
                                    $pointsText = 'очков';
                                    if(!empty($userModel)) {
                                        $points = substr($userModel->experience, strlen($userModel->experience)-1);
                                        if($points == 0) {
                                            $pointsText = 'очков';
                                        } elseif($points == 1) {
                                            $pointsText = 'очко';
                                        } elseif($points == 2 || $points == 3 || $points == 4) {
                                            $pointsText = 'очка';
                                        } elseif($points == 5 || $points == 6 || $points == 7 || $points == 8 || $points == 9) {
                                            $pointsText = 'очков';
                                        }
                                    }
                                    ?>
                                    <?= !empty($userModel) ? $userModel->experience : '0'?> <?= $pointsText; ?>
                                </div>
                            </div>
                        </div>
                        <div class="header-avatar">
                            <div class="div-avatar">
                                <a href="<?= Url::toRoute('/site/profile') ?>" class="avatar" style="background-image: url('<?= (!empty($userModel) && !empty($userModel->file_avatar)) ? "/uploads/{$userModel->file_avatar}" : '/images/default-avatar.png';?>')"></a>
                            </div>
                            <div class="profile-popup">
                                <div class="profile-popup-user_info">
                                    <div class="div-profile-popup-user-avatar">
                                        <a href="<?= Url::toRoute('/site/profile') ?>" class="profile-popup-user-avatar" style="background-image: url('<?= (!empty($userModel) && !empty($userModel->file_avatar)) ? "/uploads/{$userModel->file_avatar}" : '/images/default-avatar.png';?>')"></a>
                                    </div>
                                    <div class="profile-popup-user-content">
                                        <a href="<?= Url::toRoute('/site/profile') ?>" class="user-content-name">
                                            <?= empty(Yii::$app->user->identity) ? 'Guest' : Yii::$app->user->identity->name; ?>
                                        </a>
                                        <div class="user-content-email">
                                            <?= !empty($userModel) ? $userModel->email : '';?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-popup-menu_info">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="header-name">
                            <div class="username">
                                <?= empty(Yii::$app->user->identity) ? 'Guest' : Yii::$app->user->identity->name; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile">
                    <div class="header-mobile menu">
                        <div class="header-profile">
                            <div class="header-progress">
                                <div class="profile-level">
                                    <?= !empty($userModel) ? $userModel->level : '0'?>
                                </div>
                                <div class="profile-experience">
                                    <div class="profile-experience-progress" 
                                         data-experience-progress="<?= !empty($userModel) ? $userModel->experience : 0; ?>" 
                                         data-experience-total="<?= !empty($userModel) ? $userModel->levels->experience : 0; ?>">
                                    </div>
                                    <div class="profile-experience-show">
                                        <?php
                                        $pointsText = 'очков';
                                        if(!empty($userModel)) {
                                            $points = substr($userModel->experience, strlen($userModel->experience)-1);
                                            if($points == 0) {
                                                $pointsText = 'очков';
                                            } elseif($points == 1) {
                                                $pointsText = 'очко';
                                            } elseif($points == 2 || $points == 3 || $points == 4) {
                                                $pointsText = 'очка';
                                            } elseif($points == 5 || $points == 6 || $points == 7 || $points == 8 || $points == 9) {
                                                $pointsText = 'очков';
                                            }
                                        }
                                        ?>
                                        <?= !empty($userModel) ? $userModel->experience : '0'?> <?= $pointsText; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="header-avatar">
                                <div class="div-avatar">
                                    <a href="<?= Url::toRoute('/site/profile') ?>" class="avatar" style="background-image: url('<?php echo (!empty($userModel) && !empty($userModel->file_avatar)) ? "/uploads/{$userModel->file_avatar}" : '/images/default-avatar.png';?>')"></a>
                                </div>
                            </div>
                            <div class="header-name">
                                <div class="username">
                                    <?= empty(Yii::$app->user->identity) ? 'Guest' : Yii::$app->user->identity->name; ?>
                                </div>
                            </div>
                        </div>
                        <button class="hamburger">&#9776;</button>
                        <button class="cross">&#10006;</button>
                    </div>
                    <div class="menu-mobile">
                        <ul>
                            <?php if(Yii::$app->user->isGuest): ?>
                            <li><a href="<?= Url::toRoute('authorization') ?>"><?= 'Войти'; ?></a></li>
                            <li><a href="#"><?= 'Зарегистрироваться'; ?></a></li>
                            <?php else: ?>
                            <li><a href="<?= Url::toRoute('/site/logout') ?>"><?= 'Выйти'; ?></a></li>
                            <li><a href="<?= Url::toRoute('/site/home') ?>"><?= 'Главная'; ?></a></li>
                            <li><a href="<?= Url::toRoute('/dictionary/index') ?>"><?= 'Словарь'; ?></a></li>
                            <li><a href="<?= Url::toRoute('/site/tasks') ?>"><?= 'Тренировка'; ?></a></li>
                            <li><a href="<?= Url::toRoute('/rule/index') ?>"><?= 'Правила'; ?></a></li>
                                <?php if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN): ?>
                                <li><a href="<?= Url::toRoute('/superadmin/index') ?>"><?= 'Суперадмин'; ?></a></li>
                                <?php endif; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="main">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
<div id="cursorTooltip" class="cursorTooltip" style="visibility: hidden; position: absolute; top: 0; left: 0;">
    С такими успехами следующий уровень получишь через <strong>5</strong> дней
</div>
</html>
<?php $this->endPage() ?>
